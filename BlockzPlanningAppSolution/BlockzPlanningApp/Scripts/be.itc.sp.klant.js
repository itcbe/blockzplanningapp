﻿"use strict";
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.klant = be.itc.sp.klant || {};

be.itc.sp.klant = (function (klant) {
    var _listname = "Klant";

    klant.getListName = function () {
        return _listname;
    }

    klant.getKlantById = function (id) {
        //var deferred = $.Deferred();
        //var script = be.itc.sp.appweburl + "/Scripts/be.itc.sp.rest.js";
        //$.getScript(script, function () {
        //    var ticket = be.itc.sp.rest.getListItemById("Klant", id);
        //    ticket.done(function (data) {
        //        deferred.resolve(data);
        //    });
        //    ticket.fail(function (data) {
        //        deferred.reject(data);
        //    });
        //});
        //return deferred.promise();
        var deferred = $.Deferred();
        $.getScript(be.itc.sp.hostweburl + "/_layouts/15/SP.RequestExecutor.js", runCrossDomainRequest);
        return deferred.promise();

        function runCrossDomainRequest() {
            var url = String.format("{0}/_api/SP.AppContextSite(@target)/web/lists/getbyTitle('{1}')/Items({2})?@target='{3}'",
                be.itc.sp.getAppWebUrl(), "Klantenlijst", id, encodeURIComponent(be.itc.sp.getHostWebUrl()));
            var executor = new SP.RequestExecutor(be.itc.sp.getAppWebUrl());
            executor.executeAsync(
                {
                    url: url,
                    method: "GET",
                    headers:
                        {
                            "Accept": "application/json; odata=verbose"
                        },
                    success: successHandler,
                    error: errorHandler
                }
            );
        }

        function successHandler(data) {
            deferred.resolve(JSON.parse(data.body).d);
        }

        function errorHandler(data) {
            deferred.reject(data);
        }
    };

    return klant;

})(be.itc.sp.klant || {});

