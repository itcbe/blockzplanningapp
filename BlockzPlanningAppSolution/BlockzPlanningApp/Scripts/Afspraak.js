﻿'use strict'

$(function () {

});

function ValidateInput() {
    var result = true;
    $("#PasswordValitation").html("*").css("color","Black");
    $("#gekozenValidation").html("*").css("color", "Black");
    $("#SubjectValidation").html("*").css("color", "Black");
    $("#locationValidation").html("*").css("color", "Black");

    var pasword = $("#PaswoordTextBox").val();

    if (pasword === "") {
        result = false;
        $("#PasswordValitation").html("Verplicht!").css("color", "Red");
    }
    if ($("#GekozenListBox option").length < 1) {
        result = false;
        $("#gekozenValidation").html("Verplicht!").css("color","Red");
    }
    if ($("#OnderwerpTextBox").val() === "") {
        result = false;
        $("#SubjectValidation").html("Verplicht!").css("color", "Red");
    }
    if ($("#LocatieTextBox").val() == "") {
        result = false;
        $("#locationValidation").html("Verplicht!").css("color", "Red");
    }

    return result;
}