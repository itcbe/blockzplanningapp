﻿function GetSectoren() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Sectoren klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
            + "<Orderby>"
            + "</Query><FieldRef Name='Title' /></Orderby>"
            + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Title' />"
            + "</ViewFields>"
            + "<RowLimit>500</RowLimit>"
          + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            //var ticket = item.get_item('Ticket_x0020_voor_x0020_aangemaa');
            arraylist.push(
            {
                ID: item.get_item('ID'),
                Naam: item.get_item('Title')
            });
        }
        BindSectoren(arraylist);
        GetTechniekers();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan sectoren niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindSectoren(arraylist) {
    $('#NieuweKlantSectorSelect').empty();
    if (arraylist.length > 0) {
        var options = "<option value='0'></option>";
        for (var i = 0; i < arraylist.length; i++) {
            options += "<option value='" + arraylist[i].ID + "'>" + arraylist[i].Naam + "</option>";
        }
        $('#NieuweKlantSectorSelect').append(options);
    }
}