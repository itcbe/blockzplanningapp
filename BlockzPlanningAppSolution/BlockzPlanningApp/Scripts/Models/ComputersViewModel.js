﻿function GetComputers(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Nieuwe computer');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
              + "<Where>"
                 + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
              + "</Where>"
            + "</Query>"
            + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Klant' />"
                + "<FieldRef Name='Contactpersoon' />"
                + "<FieldRef Name='Type_x0020_computer' />"
                + "<FieldRef Name='Merk' />"
                + "<FieldRef Name='Model' />"
                + "<FieldRef Name='Wanneer_x0020_klaar_x003f_' />"
                + "<FieldRef Name='Preconfig' />"
                + "<FieldRef Name='Aantal_x0020_partities' />"
                + "<FieldRef Name='Data_x0020_overdracht' />"
                + "<FieldRef Name='Operating_x0020_System' />"
                + "<FieldRef Name='Office_x0020_versie' />"
                + "<FieldRef Name='Bitversie' />"
                + "<FieldRef Name='Taal_x0020_operating_x0020_syste' />"
                + "<FieldRef Name='Office_x0020_taal' />"
                + "<FieldRef Name='Office_x0020_licentievorm' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Voorkeur_x0020_wachtwoord' />"
                + "<FieldRef Name='Type_x0020_login' />"
                + "<FieldRef Name='Antivirus' />"
                + "<FieldRef Name='Nieuw_x0020_scherm_x0020_nodig_x' />"
                + "<FieldRef Name='Meegekocht_x0020_randapparatuur' />"
                + "<FieldRef Name='Extra_x0020_hardware_x0020_inbou' />"
                + "<FieldRef Name='Extra_x0020_software_x003f_' />"
                + "<FieldRef Name='Opmerking_x0020_voor_x0020_insta' />"
                + "<FieldRef Name='Prijsafspraken' />"
                + "<FieldRef Name='In_x0020_gebruik' />"
                + "<FieldRef Name='Ticket_x0020_voor_x0020_aangemaa' />"
            + "</ViewFields>"
            + "<RowLimit>500</RowLimit>"
          + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var ticket = item.get_item('Ticket_x0020_voor_x0020_aangemaa');
            arraylist.push(
            {
                ID: item.get_item('ID'),
                Merk: item.get_item('Merk'),
                Model: item.get_item('Model'),
                Type: item.get_item('Type_x0020_computer'),
                WanneerKlaar: item.get_item('Wanneer_x0020_klaar_x003f_'),
                TicketAangemaakt: item.get_item('Ticket_x0020_voor_x0020_aangemaa') == false ? "Nee" : "Ja",
                Prijsafspraak: item.get_item('Prijsafspraken')
            });
        }
        BindComputers(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan nieuwe computers niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindComputers(arraylist) {
    $("#computersThead").empty();
    $("#computersTbody").empty();
    if (arraylist.length > 0) {
        var head = "<tr><th>ID</th><th>Type computer</th><th>Merk</th><th>Model</th><th>Prijs afspraken</th><th>Wanneer klaar</th><th>Ticket aangemaakt</th></tr>";
        $("#computersThead").append(head);
        for (var i = 0; i < arraylist.length; i++) {
            var computer = arraylist[i];
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Type + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Merk + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Model + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Prijsafspraak + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].WanneerKlaar + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].TicketAangemaakt + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/Open.gif' style='width: 20px;' alt='Open computer' onclick='OpenComputer(" + arraylist[i].ID + ");'/></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/Edit.png' style='width: 20px;' alt='Bewerk computer' onclick='BewerkComputer(" + arraylist[i].ID + ");'/></td>";
            if (arraylist[i].TicketAangemaakt === "Nee")
                tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='MaakComputerTicket(" + arraylist[i].ID + ");'>Maak ticket</a></td>";
            tablerow += "</tr>";
            $("#computersTbody").append(tablerow);
        }
    }
}

function OpenComputer(id) {
    if (id !== "") {
        var url = 'https://itcbe.sharepoint.com/Lists/Nieuwe computer/Dispform.aspx?ID=' + id;
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
}

function BewerkComputer(id) {
    if (id !== "") {
        var url = 'https://itcbe.sharepoint.com/Lists/Nieuwe computer/Editform.aspx?ID=' + id;
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
}

function MaakComputerTicket(id) {
    var computer = GetComputerById(id);
    //computer.done(function (computer) {
    $("#NieuweComputerVlag").val("ja");
    $("#prijsafspraak").val(computer.Prijsafspraken);
    $("#NieuwTicketOmschrijvingtxt").val("Nieuwe " + computer.Type + " " + computer.Merk + " " + computer.Model);
    $("#NieuwTicketWerkgebiedSelect").val("Laptop/desktop");
    $("#NieuwTicketStatusSelect").val("Wachten op goederen");
    $("#NieuwTicketTakenTextArea").val("https://itcbe.sharepoint.com/Lists/Nieuwe computer/DispForm.aspx?ID=" + id);
    $("#techniekerGekozenSelect").append("<option value='108'>Helpdesk ITC Belgium</option>"); // zet helpdesk als toegewezen aan
    $("#NieuwTicketTaskRadio").attr("checked", true);// zet Type ticket op Task
    $('#npIdHidden').val(computer.ID);
    ShowNieuwTicketPopup();
    //})

}

function GetComputerById(identity) {
    //var deferred = $.Deferred();
    var tabel = $("#computersTbody > tr");
    var computer = {

    };
    tabel.each(function () {
        var $tr = $(this);
        var myID = $tr.find('td:nth-child(1)').text();
        if ($tr.find('td:nth-child(1)').text() == identity) {
            var id = $tr.find('td:nth-child(1)').text();
            var type = $tr.find('td:nth-child(2)').text();
            var merk = $tr.find('td:nth-child(3)').text();
            var model = $tr.find('td:nth-child(4)').text();
            var prijsafspraken = $tr.find('td:nth-child(5)').text();
            
            computer.ID = id;
            computer.Type = type;
            computer.Merk = merk;
            computer.Model = model;
            computer.Prijsafspraken = prijsafspraken == "null" ? "" : prijsafspraken;
            //deferred.resolve(computer);
        }
    });
    return computer;
    //return deferred.promise();
}

function SetTicketAangemaaktOpTrue(id) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Nieuwe computer');
        var item = list.getItemById(id);

        item.set_item('Ticket_x0020_voor_x0020_aangemaa', 1);
        item.update();
        context.executeQueryAsync(onQuerySucceeded(true), onQueryFailed);

    }

    self.onQuerySucceeded = function (sender, args) {
        //alert('Nieuwe computer aangepast');
    }

    self.onQueryFailed = function (sender, args) {
        alert('Fout bij het schrijven van de ticket aangemaakt waarde: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}