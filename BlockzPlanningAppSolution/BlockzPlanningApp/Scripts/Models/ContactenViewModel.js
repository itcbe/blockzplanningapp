﻿function Contact() {

}

function GetContactenForNieuwTicket(klantid, form) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contacten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
          + "<Query>"
            + "<Where>"
                + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
            + "</Where>"
            + "<OrderBy><FieldRef Name='Naam' /></OrderBy>"
          + "</Query>"
          + "<ViewFields>"
                // for development
              //+ "<FieldRef Name='ID' />"
              //+ "<FieldRef Name='Title' />"
              //+ "<FieldRef Name='Voornaam' />"
              //+ "<FieldRef Name='Volledige_x0020_naam' />"
              //+ "<FieldRef Name='telefoon_x0020_op_x0020_werk' />"
              //+ "<FieldRef Name='Mobiel_x0020_nummer' />"
              //+ "<FieldRef Name='E_x002d_mailadres' />"

                // for appstore
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='FirstName' />"
              + "<FieldRef Name='Volledige_x0020_naam' />"
              + "<FieldRef Name='WorkPhone' />"
              + "<FieldRef Name='CellPhone' />"
              + "<FieldRef Name='Email' />"
           + "</ViewFields>"
          + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                //ID: item.get_item('ID'),
                //Achternaam: item.get_item('Title'),
                //Voornaam: item.get_item('Voornaam'),
                //VolledigeNaam: item.get_item('Volledige_x0020_naam'),
                //Telefoon: item.get_item('telefoon_x0020_op_x0020_werk'),
                //GSM: item.get_item('Mobiel_x0020_nummer'),
                //Email: item.get_item('E_x002d_mailadres')

                // for storeApp
                ID: item.get_item('ID'),
                Achternaam: item.get_item('Title'),
                Voornaam: item.get_item('FirstName'),
                VolledigeNaam: item.get_item('Volledige_x0020_naam'),
                Telefoon: item.get_item('WorkPhone'),
                GSM: item.get_item('CellPhone'),
                Email: item.get_item('Email')
            });
        }
        if (form == "Klachten")
            BindContactenToKlachten(arraylist);
        else
            BindContactenToSelect(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de contacten niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindContactenToSelect(arraylist) {
    $("#NieuwTicketContactSelect").empty();
    $("#NieuwTicketContactSelect").append("<option value='5'>Dummy Dummy</option>");
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].VolledigeNaam + "</option>";

            $("#NieuwTicketContactSelect").append(option);
        }
    }
}

function BindContactenToKlachten(arraylist) {
    $("#nieuweKlachtContactSelect").empty();
    $("#nieuweKlachtContactSelect").append("<option value='5'>Dummy Dummy</option>");
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].VolledigeNaam + "</option>";

            $("#nieuweKlachtContactSelect").append(option);
        }
    }
}

function LoadContacten() {
    var klantid = $("#NieuwTicketKlantSelect").val();
    GetContactenForNieuwTicket(klantid);
}

function GetContacten(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contacten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
          + "<Query>"
            + "<Where>"
                + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
            + "</Where>"
            + "<OrderBy><FieldRef Name='Volledige_x0020_naam' /></OrderBy>"
          + "</Query>"
          + "<ViewFields>"
                // deel voor de developer //
              //+ "<FieldRef Name='ID' />"
              //+ "<FieldRef Name='Title' />"
              //+ "<FieldRef Name='Voornaam' />"
              //+ "<FieldRef Name='Volledige_x0020_naam' />"
              //+ "<FieldRef Name='telefoon_x0020_op_x0020_werk' />"
              //+ "<FieldRef Name='Mobiel_x0020_nummer' />"
              //+ "<FieldRef Name='E_x002d_mailadres' />"

               // deel voor de uiteindelijke App
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='FirstName' />"
              + "<FieldRef Name='Volledige_x0020_naam' />"
              + "<FieldRef Name='WorkPhone' />"
              + "<FieldRef Name='CellPhone' />"
              + "<FieldRef Name='Email' />"
           + "</ViewFields>"
          + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            // deel voor de developer
            //var id = item.get_item('ID');
            //var achternaam = item.get_item('Title');
            //var voornaam = item.get_item('Voornaam');
            //var naam = item.get_item('Volledige_x0020_naam');
            //var telefoon = item.get_item('telefoon_x0020_op_x0020_werk');
            //var gsm = item.get_item('Mobiel_x0020_nummer');
            //var email = item.get_item('E_x002d_mailadres');


            //deel voor de uiteindelijke app
            var id = item.get_item('ID');
            var achternaam = item.get_item('Title');
            var voornaam = item.get_item('FirstName');
            var naam = item.get_item('Volledige_x0020_naam');
            var telefoon = item.get_item('WorkPhone');
            var gsm = item.get_item('CellPhone');
            var email = item.get_item('Email');

            arraylist.push(
            {
                ID: id,
                Achternaam: achternaam,
                Voornaam: voornaam,
                VolledigeNaam: naam,
                Telefoon: telefoon,
                GSM: gsm,
                Email: email
            });
        }
        BindContacten(arraylist);
        var klantnummer = $("#IDCell").text();
        //var groep = $("#groepCell").text();
        //if (typeof klantnummer !== "undefined")
        //    GetKlantenVanGroep(klantnummer, groep);

    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de contacten niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindContacten(arraylist) {
    $("#contactTbody").empty();
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            var tableRow = "<tr><td>" + arraylist[i].ID + "</td>";
            tableRow += "<td>" + arraylist[i].VolledigeNaam + "</td>";
            tableRow += "<td>" + arraylist[i].Telefoon + "</td>";
            tableRow += "<td>" + arraylist[i].GSM + "</td>";
            if (arraylist[i].Email != null) {
                var email = GetEmailAdressenFromString(arraylist[i].Email);
                tableRow += "<td>" + email + "</td>";
            }
            else {
                tableRow += "<td></td>";
            }
            tableRow += "<td><a href='#' onclick='BewerkContact(" + arraylist[i].ID + ")'><img src='../Images/pen_paper_2-512.png' alt='Bewerk Contact' style='width:18px;'/></a></td></tr>";

            $("#contactTbody").append(tableRow);
        }
    }
}

function GetEmailAdressenFromString(tekst) {
    var emails = "";
    var myEmails = tekst.split('>');
    var tempMails = [];
    for (var i = 0; i < myEmails.length; i++)
    {
        if (myEmails[i].indexOf('@') > -1)
            if (myEmails[i].indexOf("<a href") == -1)
                tempMails.push(myEmails[i]);
    }
    for (var j = 0; j < tempMails.length; j++)
    {
        var item = tempMails[j];
        if (item.indexOf('<') > -1)
        {
            item = item.substring(0, (item.indexOf('<')));
        }
        emails += "<a href='mailto:" + item + "'>" + item + "</a>";
        if (j < (tempMails.length - 1))
            emails += "</br>";
    }
    return emails;
}

function stripHTML(dirtyString) {
    var container = document.createElement('div');
    container.innerHTML = dirtyString;
    return container.textContent || container.innerText;
}

function BewerkContact(id) {
    var url = hostweburl + '/Lists/Contacten/Editform.aspx?ID=' + id;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}


/******************  NIEUW CONTACT OPSLAAN ***********************/

function IsNieuwContactValid(Contact) {
    var valid = true;
    var errortext = "<ul>";
    if (Contact.Naam == "") {
        valid = false;
        errortext += "<li>Achternaam is niet ingevult.</li>"
    }
    if (Contact.Klant.length < 1) {
        valid = false;
        errortext += "<li>Geen klant gekozen.</li>"
    }
    errortext += "</ul>";
    if (!valid)
        alert(errortext);
    return valid;
}

function GetContactFromForm() {
        var naam = $("#ContactNaamtxt").val();
        var voornaam = $("#ContactVoornaamtxt").val();
        var klantid = $("#ContactKlantSelect").val();
        var functie = $("#ContactFunctietxt").val();
        var email = $("#ContactEmailTextArea").val();
        var mobiel = $("#ConatctMobieltxt").val();
        var tWerk = $("#ContactTelefoonWerktxt").val();
        var tthuis = $("#ContactTelefoonThuistxt").val();
        var bedrijf = $("#ContactBedrijftxt").val();
        var fax = $("#ContactFaxtxt").val();
        var adres = $("#ContactAdresTextArea").val();
        var plaats = $("#ContactPlaatstxt").val();
        var provincie = $("#ContactProvincietxt").val();
        var postcode = $("#ContactPostcodetxt").val();
        var land = $("#ContactLandtxt").val();
        var website = $("#ContactWebpaginatxt").val();
        var beschrijving = $("#ContactWebpaginaBeschrijvingtxt").val();
        var notities = $("#ContactNotitieTextArea").val();
        var naamfon = $("#ContactNaamFontxt").val();
        var voornaamfon = $("#ContactVoornaamFontxt").val();

        var contact = {
            Naam: naam,
            Voornaam: voornaam,
            Klant: klantid,
            Functie: functie,
            Email: email,
            Mobiel: mobiel,
            TelefoonWerk: tWerk,
            TelefoonThuis: tthuis,
            Bedrijf: bedrijf,
            Fax: fax,
            Adres: adres,
            Plaats: plaats,
            Provincie: provincie,
            Postcode: postcode,
            Land: land,
            Webpagina: website,
            Beschrijving: beschrijving,
            Notitie: notities,
            AchternaamFon: naamfon,
            VoornaamFon: voornaamfon,
            //BedrijfsnaamFon: bedrijffon,
            //KlantOud: klantoud,
            //OudContact: oudcontact
        };
    if (IsNieuwContactValid(contact)) {
        SaveContact(contact);
    }
}

function SaveContact(contact) {
    var self = this;
    self.SaveContact = function () {
        if (contact) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Contacten');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);
            var klantlookupvalue = new SP.FieldLookupValue();
            klantlookupvalue.set_lookupId(contact.Klant);

            // for developer 
            //item.set_item("Klant", klantlookupvalue);
            //item.set_item("Title", contact.Naam);
            //item.set_item("Voornaam", contact.Voornaam);
            //item.set_item("Functie", contact.Functie);
            //item.set_item("E_x002d_mailadres", contact.Email);
            //item.set_item("Mobiel_x0020_nummer", contact.Mobiel);
            //item.set_item("telefoon_x0020_op_x0020_werk", contact.TelefoonWerk);
            //item.set_item("Telefoon_x0020_thuis", contact.TelefoonThuis);
            //item.set_item("Faxnummer", contact.Fax);
            //item.set_item("Adres", contact.Adres);
            //item.set_item("Plaats", contact.Plaats);
            //item.set_item("Provincie", contact.Provincie);
            //item.set_item("Postcode", contact.Postcode);
            //item.set_item("Land_x002f_regio", contact.Land);
            //item.set_item("Webpagina", contact.Webpagina);
            //item.set_item("Notities", contact.Notitie);
            //item.set_item("Achternaam_x0020__x0028_fonetisc", contact.AchternaamFon);
            //item.set_item("Voornaam_x0020__x0028_fonetisch_", contact.VoornaamFon);

            // for storeApp
            item.set_item("Klant", klantlookupvalue);
            item.set_item("Title", contact.Naam);
            item.set_item("FirstName", contact.Voornaam);
            item.set_item("JobTitle", contact.Functie);
            item.set_item("Email", contact.Email);
            item.set_item("CellPhone", contact.Mobiel);
            item.set_item("WorkPhone", contact.TelefoonWerk);
            item.set_item("HomePhone", contact.TelefoonThuis);
            item.set_item("WorkFax", contact.Fax);
            item.set_item("WorkAddress", contact.Adres);
            item.set_item("WorkCity", contact.Plaats);
            item.set_item("WorkState", contact.Provincie);
            item.set_item("WorkZip", contact.Postcode);
            item.set_item("WorkCountry", contact.Land);
            item.set_item("WebPage", contact.Webpagina);
            item.set_item("Comments", contact.Notitie);
            item.set_item("LastNamePhonetic", contact.AchternaamFon);
            item.set_item("FirstNamePhonetic", contact.VoornaamFon);

            item.update();
            context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
        }
    }

    self.onQuerySucceeded = function (sender, args) {
        alert("Contact opgeslagen.");
        var klantid = $("#klantenSelect").val();
        GetContacten(klantid);
        CloseNieuwContactPopup();
    }

    self.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van het contact: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.SaveContact();

}
