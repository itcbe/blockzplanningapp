﻿function ShowInterventiePopup() {
    var popupWidth = Math.round($(window).width() / 1.2);
    var popupHeight = $(window).height() - 50;
    $('#interventiePopup').dialog({
        modal: true,
        width: popupWidth,
        height: popupHeight,
        buttons: [{
            text: "Sluiten",
            click: function () {
                $(this).dialog("close");
            }
        }]
    })

    //$('#interventiePopup').removeClass('Hidden');
    //$('.overlay').removeClass('Hidden');
}

function CloseInterventiePopup() {
    if (!$('#interventiePopup').hasClass('Hidden'))
        $('#interventiePopup').addClass('Hidden');
    if (!$('.overlay').hasClass('Hidden'))
        $('.overlay').addClass('Hidden');
}



function PrintButton_Click() {
    Popup($('#printablediv').html());
}

function Popup(data) {
    var mywindow = window.open('', '', 'height=400,width=600');
    mywindow.document.write('<html><head><title>' + $('#klantselect > option:selected').text() + '</title>');
    mywindow.document.write('<link rel="stylesheet" href="../Content/App.css" type="text/css" />');
    mywindow.document.write("<style>body{font-family:'Century Gothic'; }</style>");
    mywindow.document.write('</head><body class="PrintDiv">');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}