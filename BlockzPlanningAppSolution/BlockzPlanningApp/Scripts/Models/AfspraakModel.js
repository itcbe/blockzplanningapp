﻿function SaveButton_Click() {
    var afspraak =  GetAfspraak();
    SaveAfspraak(afspraak);
}

function GetAfspraak() {
    var afspraak = {
        UserEmail: "stefan.kelchtermans@itc.be",
        Omschrijving: "Test omschrijving",
        Paswoord: "Left4dead",
        Startuur: "19/03/2015 08:00:00",
        Einduur: "19/03/2015 17:00:00",
        Onderwerp: "Test onderwerp",
        Locatie: "ITC Belgium",
        Categorie: "Standaard", 
        Geadresseerden: "stefan.kelchtermans@itc.be"
    };
    return afspraak;
}

function SaveAfspraak(afspraak) {
    //var input = JSON.stringify(afspraak);
    var mydata = { "afspraak": afspraak };
    //alert("Input:" + JSON.stringify(myafspraak));
    var url = "https://afspraakservice.itc.be/AfspraakService.svc/SaveAfspraak";

    $.ajax({
        type: 'POST',
        url: url,
        dataType: "json",
        //processData: true,
        headers: {
            "Accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        data: JSON.stringify(mydata),
        success: function (data, status, jqXHR) {
            alert("success");
        },
        error: function (err) {
            alert("Fout bij het plaatsen van een afspraak. " + err.responseText + err.statusText);
        }
    });
}

