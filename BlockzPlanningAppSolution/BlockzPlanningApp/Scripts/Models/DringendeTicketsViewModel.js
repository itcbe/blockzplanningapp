﻿function setDringend(item, ticket) {
    this.save = function () {
        var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Dringende tickets');
        var itemCreateInfo = new SP.ListItemCreationInformation();
        var listItem = list.addItem(itemCreateInfo);
        var toegewezenaan = "";
        for (var i = 0; i < ticket.ToegewezenAan.length; i++) {
            if (i === 0)
                toegewezenaan += ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
            else
                toegewezenaan += ';#' + ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
        }

        listItem.set_item("Toegewezen_x0020_aan", toegewezenaan);
        var klantlookupvalue = new SP.FieldLookupValue();
        klantlookupvalue.set_lookupId(ticket.KlantID);
        listItem.set_item("klant", klantlookupvalue);
        listItem.set_item("Ticket", "https://itcbe.sharepoint.com/Lists/Ticket/Dispform.aspx?ID=" + item.get_id());
        listItem.set_item("Opmerking", $("#NieuwTicketDringendOpmerking").val());
        listItem.set_item("Actief", true);
        listItem.update();
        context.load(listItem);
        context.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
    }
    this.onQuerySucceeded = function () {
        $("#NieuwTicketDringendOpmerking").val("");
        $("#NieuwTicketIsDringend").prop('checked', false);
        TicketIsDringend();
    }
    this.onQueryFailed = function (sender, args) {
        alert('Fout bij het toevoegen van Ticket aan Dringende tickets: ' + args.get_message() + '\n' + args.get_stackTrace());
    }
    this.save();
}