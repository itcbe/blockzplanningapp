﻿function GetTechniekers() {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var collGroup = clientContext.get_web().get_siteGroups();
        var oGroup = collGroup.getByName('IT afdeling');
        this.collUser = oGroup.get_users();
        clientContext.load(collUser);

        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );

    }

    self._onLoadListSucceeded = function (sender, args) {
        var userInfo = '';
        var userEnumerator = collUser.getEnumerator();
        var arraylist = [];
        while (userEnumerator.moveNext()) {
            var oUser = userEnumerator.get_current();

            arraylist.push(
            {
                ID: oUser.get_id(),
                Titel: oUser.get_title(),
                LoginName: oUser.get_loginName(),
                Email: oUser.get_email() 
            });
        }
        BindTechniekers(arraylist);
        //GetTicketbehandelingen();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de techniekers niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindTechniekers(arraylist) {
    //// Zorg er voor dat be.itc.sp.user.js geladen is
    //var script = be.itc.sp.getAppWebUrl() + "/Scripts/be.itc.sp.user.js";
    //$.getScript(script, function () {
    //    // Wanneer deze geladen is bind dan de arraylist aan de variabele
    //    be.itc.sp.user.setUsers(arraylist);
    //});
    be.itc.sp.user.setUsers(arraylist);
    $("#tecniekerBeschikbaarSelect").empty();
    $("#afspraakTechniekerBeschikbaarSelect").empty();
    $('#nieuweKlachtBeschikbareUsersSelect').empty();
    $("#uitgevoerdDoorSelect").empty();
    $('#uitvoerderSelect').empty();
    $("#uitgevoerdDoorSelect").append("<option></option>");
    for (var i = 0; i < arraylist.length; i++) {
        var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].Titel + "</option>";
        $("#tecniekerBeschikbaarSelect").append(option);
        $("#afspraakTechniekerBeschikbaarSelect").append(option);
        $("#uitgevoerdDoorSelect").append(option);
        $('#uitvoerderSelect').append(option);
        $('#nieuweKlachtBeschikbareUsersSelect').append(option);
    }
    $('#uitvoerderSelect').prepend("<option></option>");
}