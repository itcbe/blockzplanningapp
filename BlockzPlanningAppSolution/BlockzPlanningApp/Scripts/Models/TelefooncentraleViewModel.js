﻿function GetTelefooncentrates(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Telefooncentrale');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
        + "<Where><And>"
        + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
        + "<Eq><FieldRef Name='Niet_x0020_meer_x0020_in_x0020_gebruik' /><Value Type='Boolean'>0</Value></Eq>"
        + "</And></Where>"
        + "<OrderBy><FieldRef Name='Datum_x0020_contract' Ascending='FALSE' /></OrderBy>"
        + "</Query>"
        + "<ViewFields>"
            + "<FieldRef Name='ID' />"
            + "<FieldRef Name='Klant' />"
            + "<FieldRef Name='Merk' />"
            + "<FieldRef Name='Datum_x0020_contract' />"
            + "<FieldRef Name='Niet_x0020_meer_x0020_in_x0020_gebruik' />"
            + "<FieldRef Name='Link_x0020_handleiding' />"
        + "</ViewFields>"
        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var datum = ToDateString(item.get_item('Datum_x0020_contract').toString(), true);
            var url = item.get_item('Link_x0020_handleiding').get_url();
            arraylist.push(
            {
                ID: item.get_item('ID'),
                Merk: item.get_item('Merk'),
                Datum: datum,
                Bijlage: url
            });
        }
        BindCentrale(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de telefooncentrale niet ophalen!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindCentrale(arraylist) {
    $("#centraleThead").empty();
    $("#centraleTbody").empty();
    if (arraylist.length > 0) {
        var head = "<tr><th>ID</th><th>Merk</th><th>Datum contract</th><th>Bijlage</th></tr>";
        $("#centraleThead").append(head);
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Merk + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Datum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='" + arraylist[i].Bijlage + "'>" + arraylist[i].Bijlage + "</a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/Open.gif' alt='Open centrale' style='width:23px;' onclick='OpenTelefoonCentrale(" + arraylist[i].ID + ")'/></td></tr>";

            $("#centraleTbody").append(tablerow);
        }
    }
}

function OpenTelefoonCentrale(id) {
    if (id !== "") {
        var url = 'https://itcbe.sharepoint.com/Telefooncentrale/Forms/EditForm.aspx?ID=' + id;
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
}