﻿// Globale item variabele om ticket_Id van een nieuw ticket te kunnen inlezen
var item = null;

function GetTicketsByKlant(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And>"
                        + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                        + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "</ViewFields>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Prioriteit' />"
                        + "</OrderBy>"
                        + "<RowLimit>1000</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contactitem = item.get_item('Contact');
            var contact = "";
            if (contactitem)
                contact = contactitem.get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false);
            var prioriteit = item.get_item('Prioriteit');
            prioriteit = prioriteit.substr(0, 1);
            arraylist.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: myklantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor
            });
        }
        BindKlantTickets(arraylist);
        GetKlantenByID(klantid)
        if (firstTime) {
            $("#ticketTable").tablesorter({
                sortList: [[2, 1]]
            });
            $("#ticketTable").trigger("updateAll");
            firstTime = false;
        }
        else
            $("#ticketTable").trigger("updateAll");
        $('#datumLabel').text($('#interventieDatumTxt').val());
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlantTickets(arraylist) {
    $("#ticketTbody").empty();
    $('#interventieTBody').empty();
    //// Zorg er eerst voor dat het be.itc.sp.planning.appointment.js geladen is ...
    //var script = be.itc.sp.getAppWebUrl() + "/Scripts/be.itc.sp.planning.appointment.js";
    //$.getScript(script, function () {
    $('#ticketThead').empty();
    $('#ticketThead').append("<tr>"
                        + "<th>Nummer</th>"
                        + "<th>Omschrijving</th>"
                        + "<th>Prioriteit</th>"
                        + "<th>Toegewezen aan</th>"
                        + "<th>Contact persoon</th>"
                        + "<th>Opvolgingsdatum</th>"
                        + "<th>Status</th>"
                        + "<th>Werkgebied</th>"
                        + "<th>Afdeling</th>"
                        + "<th>Laatst gewijzigd</th>"
                        + "<th>Door</th>"
                    + "</tr>");

    if (arraylist.length > 0) {
        var j = 0;
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Afdeling + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;;'>" + arraylist[i].Gewijzigd + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].GewijzigdDoor + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return NieuwAppointment(" + arraylist[i].ID + ");' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='be.itc.sp.planning.appointment.openAppointmentDialog(" + arraylist[i].ID + ")' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return CopyLink(" + arraylist[i].ID + ");' ><img src='../Images/Klembord.gif' alt='Bewerk ticket' width='30' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + arraylist[i].ID + ");' ><img src='../Images/Open.gif' alt='Open ticket' width='23' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return BewerkTicket(" + arraylist[i].ID + ");' ><img src='../Images/Edit.png' alt='Bewerk ticket' width='18' /></a></td>";
            if (arraylist[i].Bijlagen) {
                tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/paperclip.png' alt='Bewerk ticket' width='18' /></td>";
            } else {
                tablerow += "<td style='padding:6px; margin: 3px;'></td>";
            }
            tablerow += "</tr>";
            //tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='TG' value='" + tickets[i].ToegewezenAan + "' /></td></tr>";

            $("#ticketTbody").append(tablerow);

            var opvolg = getCorrectDate(arraylist[i].Opvolgdatum);//ToNormalDate(new Date(arraylist[i].Opvolgdatum), true);
            var toegewezenaan = GetInitials(arraylist[i].ToegewezenAan);


            //interventie tickets
            var tablerow1 = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + opvolg + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + toegewezenaan + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'><input type='checkbox'/> Opgelost <br />";
            tablerow1 += "<input type='checkbox' /> Niet opgelost <br />";
            tablerow1 += "<input type='checkbox' /> N.v.t </td>";
            tablerow1 += "<td></td>";
            tablerow1 += "<td></td></tr>";
            $('#interventieTBody').append(tablerow1);
        }
    }
    //});
    $("#ticketTbody tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
}

function getCorrectDate(dateStr) {
    var vandaag = new Date();
    var effectieveDatum = new Date(dateStr);
    if (effectieveDatum < vandaag) {
        return ToNormalDate(vandaag, true);
    }
    else {
        return ToNormalDate(effectieveDatum, true);
    }
}

function Groupticket_Click() {
    var group = $("#groupLink").text();
    GetTicketsByGroup(group);
}

function GetTicketsByGroup(Group) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                        + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Klant_x003a_Groep_x0020_tekst' /><Value Type='Text'>" + Group + "</Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "<FieldRef Name='Attachments' />"
                        + "<FieldRef Name='Klant_x003a_Groep_x0020_tekst' />"
                        + "</ViewFields>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Klant' />"
                        + "</OrderBy>"
                        + "<RowLimit>1000</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contact').get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false);
            var prioriteit = item.get_item('Prioriteit');
            var bijlagen = item.get_item('Attachments');
            prioriteit = prioriteit.substr(0, 1);
            arraylist.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: myklantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor,
                Bijlagen: bijlagen
            });
        }
        BindGroupTickets(arraylist);
        GetKlantenByID(myklantid)
        if (firstTime) {
            $("#ticketTable").tablesorter({
                sortList: [[2, 1]]
            });
            $("#ticketTable").trigger("updateAll");
            firstTime = false;
        }
        else
            $("#ticketTable").trigger("updateAll");
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindGroupTickets(arraylist) {
    $("#ticketTbody").empty();
    $('#ticketThead').empty();
    $('#interventieTBody').empty();
    //// Zorg er eerst voor dat het be.itc.sp.planning.appointment.js geladen is ...
    //var script = be.itc.sp.getAppWebUrl() + "/Scripts/be.itc.sp.planning.appointment.js";
    //$.getScript(script, function () {
    $('#ticketThead').append("<tr>"
                        + "<th>Nummer</th>"
                        + "<th>Klant</th>"
                        + "<th>Omschrijving</th>"
                        + "<th>Prioriteit</th>"
                        + "<th>Toegewezen aan</th>"
                        + "<th>Contact persoon</th>"
                        + "<th>Opvolgingsdatum</th>"
                        + "<th>Status</th>"
                        + "<th>Werkgebied</th>"
                        + "<th>Afdeling</th>"
                        + "<th>Laatst gewijzigd</th>"
                        + "<th>Door</th>"
                    + "</tr>");


    if (arraylist.length > 0) {
        var j = 0;
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Klant + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Afdeling + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;;'>" + arraylist[i].Gewijzigd + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].GewijzigdDoor + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return NieuwAppointment(" + arraylist[i].ID + ");' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='be.itc.sp.planning.appointment.openAppointmentDialog(" + arraylist[i].ID + ")' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return CopyLink(" + arraylist[i].ID + ");' ><img src='../Images/Klembord.gif' alt='Bewerk ticket' width='30' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + arraylist[i].ID + ");' ><img src='../Images/Open.gif' alt='Open ticket' width='23' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return BewerkTicket(" + arraylist[i].ID + ");' ><img src='../Images/Edit.png' alt='Bewerk ticket' width='18' /></a></td>";
            if (arraylist[i].Bijlagen) {
                tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/paperclip.png' alt='Bewerk ticket' width='18' /></td>";
            } else {
                tablerow += "<td style='padding:6px; margin: 3px;'></td>";
            }
            tablerow += "</tr>";
            //tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='TG' value='" + tickets[i].ToegewezenAan + "' /></td></tr>";

            $("#ticketTbody").append(tablerow);

            var opvolg = getCorrectDate(arraylist[i].Opvolgdatum);
            var toegewezenaan = GetInitials(arraylist[i].ToegewezenAan);

            var tablerow1 = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + opvolg + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'>" + toegewezenaan + "</td>";
            tablerow1 += "<td style='padding:6px; margin: 3px;'><input type='checkbox'/> Opgelost <br />";
            tablerow1 += "<input type='checkbox' /> Niet opgelost <br />";
            tablerow1 += "<input type='checkbox' /> N.v.t </td>";
            tablerow1 += "<td></td>";
            tablerow1 += "<td></td></tr>";
            $('#interventieTBody').append(tablerow1);
        }
    }
    //});
    $("#ticketTbody tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
}

function OpenTicket(ticketID) {
    var url = be.itc.sp.getHostWebUrl() + '/Lists/Ticket/Dispform.aspx?ID=' + ticketID;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

function BewerkTicket(ticketID) {
    var url = be.itc.sp.getHostWebUrl() + '/Lists/Ticket/Editform.aspx?ID=' + ticketID;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

function NieuwAppointment(id) {
    $(".overlay").removeClass('Hidden');
    $("#AgendapuntPopUp").removeClass('Hidden');
}

function HideNieuwAppointmentPopup() {
    $(".overlay").addClass('Hidden');
    $("#AgendapuntPopUp").addClass('Hidden');
}

function FilterTickets() {
    var filtertext = $("#ticketzoektxt").val();
    var tabel = $("#ticketTbody tr");
    if (filtertext == "") {
        tabel.show();
        return;
    }

    tabel.hide();

    tabel.filter(function (i, v) {
        var $t = $(this);
        if ($t.text().toLowerCase().indexOf(filtertext.toLowerCase()) >= 0) {
            return true;
        }
        return false;
    }).show();
}

function GetInitials(users) {
    var userarray = users.split(';');
    var result = "";
    var aantal = userarray.length;
    if (aantal > 0) {
        for (var i = 0; i < aantal - 1; i++) {
            var naam = userarray[i];
            if (naam[0] == ' ')
                naam = naam.substr(1, naam.length);
            var initial = getInitial(naam);
            if ((aantal - 2) == i) {
                result += initial;
            }
            else {
                result += initial + ", ";
            }
        }
    }
    return result;
}

function getInitial(naam) {
    var result = "";
    switch (naam) {
        case "Carlo Daniels":
            result = "CD";
            break;
        case "Davy Craenen":
            result = "DC";
            break;
        case "Davy Janssen":
            result = "DJ";
            break;
        case "Dietmar Braem":
            result = "DB";
            break;
        case "Frank Vanrusselt":
            result = "FV";
            break;
        case "Glenn Vanderborght":
            result = "GV";
            break;
        case "Helpdesk ITC Belgium":
            result = "Helpdesk";
            break;
        case "Inneke Ponet":
            result = "IP";
            break;
        case "Johan Reenaers":
            result = "JR";
            break;
        case "Kristof Dirkx":
            result = "KD";
            break;
        case "Kristof Herten":
            result = "KH";
            break;
        case "Kristof Peters":
            result = "KP";
            break;
        case "Mario Derese":
            result = "MD";
            break;
        case "Mark Meerten":
            result = "MM";
            break;
        case "Martijn  Valkenborgh":
            result = "MV";
            break;
        case "Planning ITC Belgium":
            result = "Planning";
            break;
        case "Ron Stevens":
            result = "RS";
            break;
        case "Sander Verweij":
            result = "SV";
            break;
        case "Stefan Cardinaels":
            result = "SC";
            break;
        case "Stefan Kelchtermans":
            result = "SK";
            break;
    }
    return result;
}

//todo function getInitial

/********************* Opslaan nieuw ticket ***************************/

function IsTicketValid(ticket) {
    var valid = true;
    var errortext = "<ul>";
    if (ticket.KlantID === "") {
        valid = false;
        errortext += "<li>Geen klant gekozen!</li>";
    }
    if (ticket.ContactID === "") {
        valid = false;
        errortext += "<li>Geen contact gekozen!</li>";
    }
    if (ticket.Omschrijving === "") {
        valid = false;
        errortext += "<li>Geen Korte omschrijving ingegeven!</li>";
    }
    if (ticket.Omschrijving.length > 40) {
        valid = false;
        errortext += "<li>De korte omschrijving is te lang! max 40 karakters!</li>";
    }
    if (ticket.Werkgebied === "") {
        valid = false;
        errortext += "<li>Geen werkgebied gekozen!</li>";
    }
    if (ticket.Prioriteit === "") {
        valid = false;
        errortext += "<li>Geen prioriteit gekozen!</li>";
    }
    if (ticket.ToegewezenAan.length < 1) {
        valid = false;
        errortext += "<li>Geen toegewezen aan gekozen!</li>";
    }
    if (ticket.TypeTicket === "") {
        valid = false;
        errortext += "<li>Geen type ticket gekozen!</li>";
    }
    if (ticket.Status === "") {
        valid = false;
        errortext += "<li>Geen status gekozen!</li>";
    }
    if (ticket.Opvolgdatum === "") {
        valid = false;
        errortext += "<li>Geen opvolgingsdatum gekozen!</li>";
    }
    //if (ticket.Afdeling === "") {
    //    valid = false;
    //    errortext += "<li>Geen afdeling gekozen!</li>";
    //}

    errortext += "</ul>";
    if (!valid) {
        $("#validatieDialog").append(errortext);
        $("#validatieDialog").dialog("open");
        //alert(errortext);
    }
    return valid;
}

function DeleteNieuwTicketData() {
    $("#NieuwTicketOmschrijvingtxt").val("");
    $("#NieuwTicketUrentxt").val("");
    $("#NieuwTicketTakenTextArea").val("");
    $("#techniekerGekozenSelect option").each(function () {
        var $opt = $(this);
        $("#tecniekerBeschikbaarSelect").append($opt);
    });
    var date = new Date();
    $("#NieuwTicketOpvolgDatumtxt").val(ToDateString(date.toString(), true));
    $("#NieuwTicketPrioriteitSelect").val("2 Gemiddeld");
    $("#NieuwTicketWerkgebiedSelect").val("");
    $("#NieuwTicketStatusSelect").val("Gepland");
}


function GetTicketFromForm() {
    var klantid = $("#NieuwTicketKlantSelect").val();
    var contactid = $("#NieuwTicketContactSelect").val();
    var omschrijving = $("#NieuwTicketOmschrijvingtxt").val();
    var werkgebied = $("#NieuwTicketWerkgebiedSelect").val();
    var prioriteit = $("#NieuwTicketPrioriteitSelect").val();
    var toegewezenaan = $("#techniekerGekozenSelect option");
    var toegewezenLijst = [];
    toegewezenaan.each(function () {
        $opt = $(this);
        var id = $opt.val();
        var naam = $opt.text();
        var user = { ID: id, Naam: naam };
        toegewezenLijst.push(user);
    });
    var typeticket = "";
    if ($("#NieuwTicketCallRadio").is(":checked") == true)
        typeticket = "CALL";
    else if ($("#NieuwTicketTaskRadio").is(":checked") == true) {
        typeticket = "TASK";
    }
    else {
        typeticket = "PROJECT";
    }

    var status = $("#NieuwTicketStatusSelect").val();
    var opvolgdatum = ToSharePointDate($("#NieuwTicketOpvolgDatumtxt").val());
    var uren = $("#NieuwTicketUrentxt").val();

    var tekst = CleanOmschrijving($("#NieuwTicketTakenTextArea").val());


    var taken = tekst;

    var afdeling = "";
    //if ($("#NieuwTickettechnischRadio").is(":checked") === true) {
    //    afdeling = "Technisch";
    //}
    //else if ($("#NieuwTicketSoftwareRadio").is(":checked") === true) {
    //    afdeling = "Software";
    //}
    //else {
    //    afdeling = "Technisch en software";
    //}

    var ticket = {
        KlantID: klantid,
        ContactID: contactid,
        Omschrijving: omschrijving,
        Werkgebied: werkgebied,
        Prioriteit: prioriteit,
        ToegewezenAan: toegewezenLijst,
        TypeTicket: typeticket,
        Status: status,
        //Bijlage: bijlage,
        //Bestandnaam : bestandnaam,
        Opvolgdatum: opvolgdatum,
        Uren: uren,
        Taken: taken,
        Afdeling: afdeling
    };

    if (IsTicketValid(ticket)) {
        SaveTicket(ticket);
    }
}

function CleanOmschrijving(s) {
    var temptext = s;

    temptext = temptext.replace(/\&/g, '&#38;');
    temptext = temptext.replace(/</g, "&#60;");
    temptext = temptext.replace(/>/g, "&#62;");
    temptext = temptext.replace(/@/g, "&#64;");

    var arrayTaken = temptext.split('\n');
    var result = "<br/>";
    for (var i = 0; i < arrayTaken.length; i++) {
        result += arrayTaken[i] + "<br/>";
    }
    return result;
}

function SaveTicket(ticket) {
    this.facturatieTekst = "";
    this.save = function () {
        if (ticket) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
            var itemCreateInfo = new SP.ListItemCreationInformation();
            item = list.addItem(itemCreateInfo);

            var klantlookupvalue = new SP.FieldLookupValue();
            klantlookupvalue.set_lookupId(ticket.KlantID);
            item.set_item("Klant", klantlookupvalue);

            var toegewezenaan = "";
            for (var i = 0; i < ticket.ToegewezenAan.length; i++) {
                if (i === 0)
                    toegewezenaan += ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
                else
                    toegewezenaan += ';#' + ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
            }

            item.set_item("Toegewezen_x0020_aan", toegewezenaan);

            item.set_item("Contact", ticket.ContactID);
            item.set_item("Title", ticket.Omschrijving);
            item.set_item("Werkgebied", ticket.Werkgebied);
            item.set_item("Prioriteit", ticket.Prioriteit);
            item.set_item("Type_x0020_ticket", ticket.TypeTicket);
            item.set_item("Status", ticket.Status);
            item.set_item("Opvolgingsdatum", ticket.Opvolgdatum);
            item.set_item("Voorziene_x0020_uren", ticket.Uren);

            item.set_item("Uitgevoerde_x0020_taken", ticket.Taken);
            //item.set_item("Afdeling", ticket.Afdeling);
            //item.set_item("Factureerder", ticket.Factureerder);
            //item.set_item("Attachments", ticket.Bijlage)
            item.update();
            context.load(item);
            //context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
            context.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
        }
    }
    this.onQuerySucceeded = function () {
        be.itc.sp.rest.fileupload.TicketNumber = item.get_item('ID');
        if (be.itc.sp.rest.fileupload.HasFiles()) {
            var uploadFiles = be.itc.sp.rest.fileupload.uploadFileList();
            uploadFiles.done(function (data) {
                alert("Bijlagen met succes opgeslagen.");
                be.itc.sp.rest.fileupload.ClearFileMap();
                be.itc.sp.rest.fileupload.updateFileList();
                var id = item.get_item('ID');
                if (TicketIsDringend()) {
                    setDringend(item, ticket);
                }
                
            });
            uploadFiles.fail(function (error) {
                be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het opslaan van de bijlagen.");
            });
        }
        else {
            if (TicketIsDringend()) {
                setDringend(item, ticket);
            }
        }
        alert("Ticket opgeslagen.");
        CloseTicketPopup();
        var vlag = $("#NieuweComputerVlag").val();
        if (vlag === "ja") {
            var facturatietekst = $('#facturatieCell').text();
            var klantFacturatieTekst = facturatietekst + "<br /><br />" + id + "<br />" + $("#prijsafspraak").val();
            SaveNieuwePCToKlant(ticket.KlantID, klantFacturatieTekst)
            $("#NieuweComputerVlag").val("nee");
            $("#prijsafspraak").val("");
        }

        klantselect_onchange();
    }

    this.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van het Ticket: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    this.save();
}

function CloseTicketPopup() {
    
    $("#NieuwTicketPopUp").dialog("close");
    DeleteNieuwTicketData();
}

function InsertTicket(ticket) {
    //var deferred = $.Deferred();
    var url = String.format("{0}/_api/SP.AppContextSite(@target)/web/lists/getbyTitle('{1}')/items?@target='{2}'",
    getQueryStringParameter("SPAppWebUrl"), list, encodeURIComponent(getQueryStringParameter("SPHostUrl")));

    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json;odata=verbose",
        data: JSON.stringify(ticket),
        headers: {
            "Accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            alert(JSON.parse(data.body).d.results[0].ID);
            //deferred.resolve(data);
        },
        error: function (xhr) {
            //deferred.reject(xhr);
        }
    });
    //return deferred.promise();
}