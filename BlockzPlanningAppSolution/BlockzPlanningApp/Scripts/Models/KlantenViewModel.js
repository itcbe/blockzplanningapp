﻿/*
/* Klanten dropdown elementen 
*/

function GetKlanten() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
            + "<Query>"
                + "<OrderBy><FieldRef Name='Bedrijf' /></OrderBy> "
            + "</Query>"
          + "<ViewFields>"
             + "<FieldRef Name='Bedrijf' />"
             + "<FieldRef Name='ID' />"
          + "</ViewFields>"
          + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Naam: item.get_item('Bedrijf')
            });
        }
        BindKlanten(arraylist);
        //GetTechniekers();
        GetVertegenwoordigers();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlanten(arraylist) {
    $("#klantenSelect").empty();

    if (arraylist.length > 0) {
        arraylist.sort(SortByName);
        $("#klantenSelect").append("<option value='0'>-- Kies een klant --</option>");
        for (var i = 0; i < arraylist.length; i++) {
            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].Naam + "</option>";
            $("#klantenSelect").append(option);
            $("#ContactKlantSelect").append(option);
            $("#ContactKlantOudSelect").append(option);
            $("#NieuwTicketKlantSelect").append(option);
            $('#nieuweKlachtKlantSelect').append(option);
        }
    }
    CacheItems();
}

function SortByName(a, b) {
    var aName = a.Naam.toLowerCase();
    var bName = b.Naam.toLowerCase();
    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}


/******************************  Klant details ***************************/

function GetKlantenByID(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
            + "<Query>"
            + "<Where>"
            + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + klantid + "</Value></Eq>"
            + "</Where>"
            + "</Query>"
              + "<ViewFields>"
                  + "<FieldRef Name='Bedrijf' />"
                  + "<FieldRef Name='Email' />"
                  + "<FieldRef Name='Fax' />"
                  + "<FieldRef Name='Gemeente' />"
                  + "<FieldRef Name='Huisnummer' />"
                  + "<FieldRef Name='ID' />"
                  + "<FieldRef Name='Title' />"
                  + "<FieldRef Name='Opmerking_x0020_facturatie' />"
                  + "<FieldRef Name='Postcode' />"
                  + "<FieldRef Name='Sector' />"
                  + "<FieldRef Name='Straat' />"
                  + "<FieldRef Name='Telefoon' />"
                  + "<FieldRef Name='Tweede_x0020_naam' />"
                  + "<FieldRef Name='Vertegenwoordiger' />"
                  + "<FieldRef Name='Website' />"
              + "</ViewFields>"
              + "<RowLimit>1</RowLimit>"
              + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            var vertegenwoordigerLookUp = item.get_item('Vertegenwoordiger');
            var vertegenwoordiger = "";
            if (vertegenwoordigerLookUp){
                vertegenwoordiger = vertegenwoordigerLookUp.get_lookupValue();
            }

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Bedrijf: item.get_item('Bedrijf'),
                Nummer: item.get_item('Title'),
                Adres: item.get_item('Straat') + " " + item.get_item('Huisnummer') + " " + item.get_item('Postcode') + " " + item.get_item('Gemeente'),
                Telefoon: item.get_item('Telefoon'),
                Vertegenwoordiger: vertegenwoordiger,
                OpmFacturatie: item.get_item('Opmerking_x0020_facturatie')
            });
        }
        BindKlantDetails(arraylist);
        GetContacten(klantid);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlantDetails(arraylist) {
    if (arraylist.length > 0) {
        $("#IDCell").html(arraylist[0].Nummer);
        $("#bedrijfCell").html(arraylist[0].Bedrijf);
        $("#facturatieCell").html(arraylist[0].OpmFacturatie);
        //$("#opmklantCell").html(arraylist[0].OpmKlant);
        $("#adresCell").html(arraylist[0].Adres);
        $("#telefoonCell").html(arraylist[0].Telefoon);
        //$("#hostingCell").html(arraylist[0].Hosting);
        $("#vertegenwoordigertCell").html(arraylist[0].Vertegenwoordiger);
        //$("#opmLocatieCell").html(arraylist[0].OpmLocatie);
        //$("#groepCell").html("<a href='#' id='groupLink' onclick='OpenGroupPopUp();'>" + arraylist[0].Groep + "</a>");
        //$("#techniekerCell").html(arraylist[0].Technieker);
    }
    else {
        $("#IDCell").html("");
        $("#bedrijfCell").html("");
        $("#facturatieCell").html("");
        //$("#opmklantCell").html("");
        $("#adresCell").html("");
        $("#telefoonCell").html("");
        //$("#hostingCell").html("");
        $("#vertegenwoordigertCell").html("");
        //$("#opmLocatieCell").html("");
        //$("#groepCell").html("");
        //$("#techniekerCell").html("");
    }
}

/************************ Klanten van dezelfde groep  *******************************/
function GetKlantenVanGroep(klantnummer, groep) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

        var caml = "<View>";
        caml += "<Query>";
        caml += "<Where>";
        if (!groep || groep !== "")
            caml += "<Eq><FieldRef Name='Groep' /><Value Type='Choice'>" + groep + "</Value></Eq>";
        else
            caml += "<Eq><FieldRef Name='Title' /><Value Type='Text'>" + klantnummer + "</Value></Eq>";
        caml += "</Where>";
        caml += "<OrderBy><FieldRef Name='Naam1' /></OrderBy>";
        caml += "</Query>";
        caml += "<ViewFields>";
        caml += "<FieldRef Name='ID' />";
        caml += "<FieldRef Name='Title' />";
        caml += "<FieldRef Name='Naam1' />";
        caml += "<FieldRef Name='Locatie' />";
        caml += "<FieldRef Name='Groep' />";
        caml += "</ViewFields>";
        caml += "</View>";
        

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Nummer: item.get_item('Title'),
                Naam: item.get_item('Naam1'),
                Locatie: item.get_item('Locatie'),
                Groep: item.get_item('Groep')
            });
        }
        BindGroup(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindGroup(arraylist) {
    $("#GroepDiv").empty();
    if (arraylist.length > 0) {

        var table = "<table><thead><tr><th>ID</th><th>Nummer</th><th>Naam</th><th>Locatie</th></tr></thead><tbody id='groepTbody'>";
        for (var i = 0; i < arraylist.length; i++) {
            table += "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            table += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Nummer + "</td>";
            table += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Naam + "</td>";
            table += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Locatie + "</td>";
            table += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='OpenGroepKlant(" + arraylist[i].ID + ")'><img src='../Images/pen_paper_2-512.png' style='width:18px;'/></a></td></tr>";
        }
        table += "</tbody></table>";
        $("#GroepDiv").append(table);
    }
    else
        $("#GroepDiv").append("<p>Geen andere leden in deze groep.</p>");
}

function OpenGroepKlant(id) {
    var url = getQueryStringParameter("SPHostUrl") + '/Lists/Klanten/Editform.aspx?ID=' + id;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

/************************ nieuwe Klant opslaan *********************/

function IsKlantValid(klant) {
    var valid = true;
    var errortext = "<ul>";
    if (klant.Nummer === "") {
        valid = false;
        errortext += "<li>Geen nummer ingevult!</li>";
    }
    if (klant.bedrijf === "") {
        valid = false;
        errortext += "<li>Geen bedrijfsnaam ingevult!</li>";
    }
    if (klant.Adres === "") {
        valid = false;
        errortext += "<li>Geen adres ingevult!</li>";
    }

    if (klant.Huisnummer === "") {
        valid = false;
        errortext += "<li>Geen huisnummer ingevuld!</li>";
    }
    if (klant.Postcode === "") {
        valid = false;
        errortext += "<li>Geen postcode ingevult!</li>";
    } if (klant.Gemeente === "") {
        valid = false;
        errortext += "<li>Geen locatie ingevult!</li>";
    }
    if (klant.Vertegenwoordiger === "") {
        valid = false;
        errortext += "<li>Geen vertegenwoordiger ingevult!</li>";
    }
    errortext += "</ul>";
    if (!valid)
        alert(errortext);
    return valid;
}

function GetKlantFromForm() {
    var nummer = $("#NieuweKlantNummertxt").val();
    var bedrijf = $("#NieuweKlantBedrijftxt").val();
    var tweedeNaam = $('#NieuweKlantTweedeNaamtxt').val();
    var adres = $("#NieuweKlantAdrestxt").val();
    var huisnummer = $("#NieuweKlantHuisnummertxt").val();
    var postcode = $("#NieuweKlantPostcodetxt").val();
    var gemeente = $("#NieuweKlantLocatietxt").val();
    var telefoon = $("#NieuweKlantTelefoontxt").val();
    var fax = $("#NieuweKlantFaxtxt").val();
    var email = $('#NieuweKlantEmailtxt').val();
    var website = $('#NieuweKlantWebsitetxt').val();
    var vertegenwoordiger = $("#NieuweKlantVertegenwoordigerSelect").text();
    var vertegenwoordigerID = $("#NieuweKlantVertegenwoordigerSelect").val();
    var sectorID = $("#NieuweKlantSectorSelect").val();
    var sector = $("#NieuweKlantSectorSelect").text();
    var notities = $('#NieuweKlantAlgemeneOpmTextArea').val();
    var opmerkingfac = $("#NieuweKlantOpmFactuurtxt").val();
    var klant = {
        Nummer: nummer,
        Bedrijf: bedrijf,
        TweedeNaam: tweedeNaam,
        Adres: adres,
        Huisnummer: huisnummer,
        Postcode: postcode,
        Gemeente: gemeente,
        Telefoon: telefoon,
        Fax: fax,
        Email: email,
        Website: website,
        Vertegenwoordiger: vertegenwoordiger,
        VertegenwoordigerID: vertegenwoordigerID,
        Sector: sector,
        SectorID: sectorID,
        //Service: service,
        Notities: notities,
        OpmerkingFactuur: opmerkingfac
        //TypeFacturatie: typefacturatie,
        //Hosting: hosting,
        //OpmerkingKlant: opmerkingklant,
        //Technieker: technieker,
        //OpmerkingLocatie: opmerkinglocatie,
        //Groep: groep,
        //Hoofdzetel: hoofdzetel,
        //Ticketbehandeling: ticketbehandeling
    };

    if (IsKlantValid(klant)) {
        SaveKlant(klant);
    }
}

function SaveKlant(klant) {
    var self = this;
    self.SaveKlant = function () {
        if (klant) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);


            var vertegenwoordigerLookUp = new SP.FieldLookupValue();
            vertegenwoordigerLookUp.set_lookupId(klant.VertegenwoordigerID);

            var sectorLookUp = new SP.FieldLookupValue();
            sectorLookUp.set_lookupId(klant.SectorID);

            item.set_item("Title", klant.Nummer);
            item.set_item("Bedrijf", klant.Bedrijf);
            item.set_item("Tweede_x0020_naam", klant.TweedeNaam);
            item.set_item("Straat", klant.Adres);
            item.set_item("Huisnummer", klant.Huisnummer);
            item.set_item("Postcode", klant.Postcode);
            item.set_item("Gemeente", klant.Gemeente);
            item.set_item("Telefoon", klant.Telefoon);
            item.set_item("Fax", klant.Fax);
            item.set_item("Email", klant.Email);
            item.set_item("Website", klant.Website);
            if (klant.VertegenwoordigerID != "0")
                item.set_item("Vertegenwoordiger", vertegenwoordigerLookUp);
            if (klant.SectorID !="0")
                item.set_item("Sector", sectorLookUp);
            item.set_item("Algemene_x0020_opmerking", klant.Notities);
            item.set_item("Opmerking_x0020_facturatie", klant.OpmerkingFactuur);

            item.update();
            context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
        }
    }

    self.onQuerySucceeded = function (sender, args) {
        alert("Klant opgeslagen.");
        GetKlanten();
        CloseNieuweKlantPopup();
    }

    self.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van de Klant: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.SaveKlant();
}

//******************************** TicketBehandeling ophalen ************************************************/

function GetTicketbehandelingen() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticketbehandeling');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                          + "<ViewFields>" 
                            + "<FieldRef Name='ID' />"
                            + "<FieldRef Name='Title' />"
                          + "</ViewFields>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Behandeling: item.get_item('Title')
            });
        }
        BindData(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de ticketbehandelingen niet ophalen!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindData(arraylist) {
    $("#ticketbehandelingSelect").empty();
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            $("#ticketbehandelingSelect").append("<option value='" + arraylist[i].ID + "'>" + arraylist[i].Behandeling + "</option>");
        }
    }
}

function SaveNieuwePCToKlant(klantid, tekst) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');
        var item = list.getItemById(klantid);

        item.set_item('Opmerkingen_x0020_ivm_x0020_fact', tekst);
        item.update();
        context.executeQueryAsync(onKlantQuerySucceeded(true), onKlantQueryFailed);

    }

    self.onKlantQuerySucceeded = function (sender, args) {
        //alert('Facturatie tekst aangepast');
        var id = $('#npIdHidden').val();
        SetTicketAangemaaktOpTrue(id);
    }

    self.onKlantQueryFailed = function (sender, args) {
        alert('Fout bij het schrijven van de facturatietekst: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}