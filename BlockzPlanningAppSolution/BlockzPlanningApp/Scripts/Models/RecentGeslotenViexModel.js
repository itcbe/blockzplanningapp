﻿function GetRecentGesloten(klantid, datum) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
                + "<Where>"
                    + "<And>"
                         + "<And>"
                             + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                             + "<Eq><FieldRef Name='Status'/><Value Type='Text'>Afgesloten</Value></Eq>"
                         + "</And>"
                         + "<Gt><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='TRUE'>" + datum + "</Value></Gt>"
                    + "</And>"
                + "</Where>"
                + "<OrderBy><FieldRef Name='Modified' Ascending='FALSE' /></OrderBy>"
            + "</Query>"
            + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Type_x0020_ticket' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Prioriteit' />"
                + "<FieldRef Name='Status' />"
                + "<FieldRef Name='Werkgebied' />"
                + "<FieldRef Name='Afdeling' />"
                + "<FieldRef Name='Klant' />"
                + "<FieldRef Name='Toegewezen_x0020_aan' />"
                + "<FieldRef Name='Opvolgingsdatum' />"
                + "<FieldRef Name='Contact' />"
                + "<FieldRef Name='Modified' />"
                + "<FieldRef Name='Editor' />"
            + "</ViewFields>"
         + "</View>");


        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contact').get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false);
            var prioriteit = item.get_item('Prioriteit');
            prioriteit = prioriteit.substr(0, 1);
            arraylist.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: myklantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor
            });

        }
        BindRecentGesloten(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan recent gesloten tickets niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindRecentGesloten(arraylist) {
    $("#recentThead").empty();
    $("#recentTbody").empty();
    if (arraylist.length > 0) {
        var head = "<tr><th>ID</th><th>Klant</th><th>Omschrijving</th><th>Prioriteit</th><th>Toegewezen aan</th>"
                 + "<th>Contact</th><th>Opvolgingsdatum</th><th>Status</th><th>Werkgebied</th><th>Laatst gewijzigd</th><th>Door</th></tr>";
        $("#recentThead").append(head);
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Klant + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Gewijzigd + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].GewijzigdDoor + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return CopyLink(" + arraylist[i].ID + ");' ><img src='../Images/Klembord.gif' alt='Bewerk ticket' width='30' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + arraylist[i].ID + ");' ><img src='../Images/Open.gif' alt='Open ticket' width='23' /></a></td>";

            $("#recentTbody").append(tablerow);
        }
    }
}