﻿function GetKlachten(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klachten klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
          + "<Where>"
            + "<And>"
              + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
              + "<Neq><FieldRef Name='Status' /><Value Type='Choise'>Afgesloten</Value></Neq>"
            + "</And>"
          + "</Where>"
          + "</Query>"
          + "<ViewFields>"
          + "<FieldRef Name='ID' />"
            + "<FieldRef Name='Title' />"
            + "<FieldRef Name='Factuurnummer_x0028_s_x0029_' />"
            + "<FieldRef Name='Facturatiedatum' />"
            + "<FieldRef Name='Toegewezen_x0020_aan' />"
            + "<FieldRef Name='Status' />"
            + "<FieldRef Name='Klant' />"
            + "<FieldRef Name='Contacten'/>"
          + "</ViewFields>"
          + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            //var klant = item.get_item('Klant').get_lookupValue();
            //var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contacten').get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }

            //var factuurdatum = item.get_item('Facturatiedatum').toString();
            //var myfactuurdatum = ToDateString(factuurdatum, false);


            arraylist.push(
            {
                ID: item.get_item('ID'),
                Titel: item.get_item('Title'),
                FactuurNummer: item.get_item('Factuurnummer_x0028_s_x0029_'),
                //Factuurdatum: myfactuurdatum,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                //Klant: klant,
                Contact: contact
            });
        }
        BindKlachten(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de klachten niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlachten(arraylist) {
    $("#klachtenThead").empty();
    $("#klachtenTbody").empty();
    if (arraylist.length > 0) {
        var head = "<tr><th>ID</th><th>Omschrijving</th><th>Factuurnummer</th><th>Toegewezen aan</th><th>Status</th><th>Contact</th></tr>";
        $("#klachtenThead").append(head);

        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Titel + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].FactuurNummer + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/Open.gif' onclick='OpenKlacht(" + arraylist[i].ID + ");' style='width:20px;'/></td></tr>";
            $("#klachtenTbody").append(tablerow);
        }
    }
}

function OpenKlacht(id) {
    if (id !== "") {
        var url = be.itc.sp.getHostWebUrl() + '/Lists/Klachten klanten/Dispform.aspx?ID=' + id;
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
}

function nieuweKlachtKlanteSelect_OnChange() {
    var klantid = $('#nieuweKlachtKlantSelect').val();
    GetContactenForNieuwTicket(klantid, "Klachten");
}

function selecteerUser_click() {
    var option = $('#nieuweKlachtBeschikbareUsersSelect option:selected');
    $('#nieuweKlachtelectedUsers').append(option);
    $("#nieuweKlachtBeschikbareUsersSelect option[value='" + option.text() + "']").remove();
    SortSelect($("#nieuweKlachtelectedUsers > option"), $("#nieuweKlachtelectedUsers"));
}

function deselectUser_click() {
    var option = $('#nieuweKlachtelectedUsers option:selected');
    $('#nieuweKlachtBeschikbareUsersSelect').append(option);
    $("#nieuweKlachtelectedUsers option[value='" + option.text() + "']").remove();
    SortSelect($("#nieuweKlachtBeschikbareUsersSelect > option"), $("#nieuweKlachtBeschikbareUsersSelect"));
}

function SortSelect(select, element) {
    select.sort(function (a, b) {
        //return $(a).text > $(b).text;
        if (a.text > b.text) {
            return 1;
        }
        else if (a.text < b.text) {
            return -1;
        }
        else {
            return 0
        }

    });
    $(element).empty().append(select);
}


// nieuwe klacht opslaan

function GetKlachtFromForm() {
    var omschrijving = $('#nieuweKlachtOmschrijvingTxt').val();
    var klant = $('#nieuweKlachtKlantSelect option:selected').text();
    var klantid = $('#nieuweKlachtKlantSelect').val();
    var datum = ToSharePointDate($('#nieuweKlachtFacturatiedatumTxt').val());
    var factureerder = $('#nieuweKlachtFactureerderSelect option:selected').text();
    var factuurnummers = $('#nieuweKlachtFactuurNummersTxt').val();
    var contact = $('#nieuweKlachtContactSelect option:selected').text();
    var contactid = $('#nieuweKlachtContactSelect').val();
    var mail = $('#nieuweKlachtMailTextArea').val();
    var status = $('#nieuweKlachtStatusSelect option:selected').text();
    var gekozen = $('#nieuweKlachtelectedUsers option');
    var werken = $('#nieuweKlachtWerkenTextArea').val();
    var toegewezenaan = "";
    GetUserID(function (group) {
        var teller = 0;
        for (var i = 0; i < group.length; i++) {
            for (var j = 0; j < gekozen.length; j++) {
                var user = group[i].get_title();
                var current = gekozen[j].innerText;
                if (user === current) {
                    console.log(group[i].get_title() + ', ' + group[i].get_id());
                    if (teller === 0)
                        toegewezenaan += group[i].get_id() + ';#' + group[i].get_title();
                    else
                        toegewezenaan += ';#' + group[i].get_id() + ';#' + group[i].get_title();
                    teller++;
                }
            }
        }
        var klacht = {
            Omschrijving: omschrijving,
            Klant: klant,
            KlantId: klantid,
            Facturatiedatum: datum,
            Factureerder: factureerder,
            Factuurnummers: factuurnummers,
            Contact: contact,
            ContactId: contactid,
            Mail: mail,
            Status: status,
            ToegewezenAan: toegewezenaan,
            Werken: werken
        };

        if (ValidateKlacht(klacht)) {
            SaveKlacht(klacht);
        }
    }); 
}

function ValidateKlacht(klacht) {
    var valid = true;
    var errortext = "";
    if (klacht.Omschrijving == "") {
        valid = false;
        errortext += "- Korte omschrijving niet ingevuld!\n";
    }
    if (klacht.Klant == "") {
        valid = false;
        errortext += "- Geen klant gekozen!\n";
    }
    if (klacht.Status == "") {
        valid = false;
        errortext += "- Geen status gekozen!\n";
    }
    if (klacht.ToegewezenAan === "") {
        valid = false;
        errortext += "- Geen toegewezen aan gekozen!\n";
    }

    if (!valid)
        alert(errortext);

    return valid;
}

function SaveKlacht(klacht) {
    var self = this;
    self.SaveKlant = function () {
        if (klacht) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Klachten klanten');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);

            var klant = new SP.FieldLookupValue();
            klant.set_lookupId(klacht.KlantId);

            var contact = new SP.FieldLookupValue();
            contact.set_lookupId(klacht.ContactId);

            item.set_item("Title", klacht.Omschrijving);
            item.set_item("Klant", klant);
            item.set_item("Facturatiedatum", klacht.Facturatiedatum);
            //item.set_item("Factureerder", klacht.Factureerder);
            item.set_item("Factuurnummer_x0028_s_x0029_", klacht.Factuurnummers);
            item.set_item("Contacten", contact);
            item.set_item("Mail_x002f_klacht_x0020_klant", klacht.Mail);
            item.set_item("Status", klacht.Status);
            item.set_item("Toegewezen_x0020_aan", klacht.ToegewezenAan);
            item.set_item("Uitgevoerde_x0020_werken", klacht.Werken);

            item.update();
            context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
        }
    }

    self.onQuerySucceeded = function (sender, args) {
        alert("Klacht opgeslagen.");
        GetKlachten(klacht.KlantId)
        CloseNieuweKlachtPopup();
    }

    self.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van de Klacht: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.SaveKlant();
}

function GetUserID(succes, error) {
    var oGroup = context.get_web().get_siteGroups();
    itcGroup = oGroup.getByName("IT afdeling");
    usercol = itcGroup.get_users();
    context.load(usercol);

    context.executeQueryAsync(function () {
        var result = [];
        var e = usercol.getEnumerator();
        while (e.moveNext()) {
            result.push(e.get_current());
        }
        succes(result);
    },
    error);
}

function ClearKlachtForm() {
    $('#nieuweKlachtOmschrijvingTxt').val("");
    //$('#nieuweKlachtFacturatiedatumTxt').val(new Date());
    $('#nieuweKlachtFactureerderSelect').val($('#nieuweKlachtFactureerderSelect option:first').val());
    $('#nieuweKlachtFactuurNummersTxt').val("");
    $('#nieuweKlachtContactSelect').val($('#nieuweKlachtContactSelect option.first').val());
    $('#nieuweKlachtMailTextArea').val("");
    $('#nieuweKlachtStatusSelect').val( $('#nieuweKlachtStatusSelect option:first').val());
    $('#nieuweKlachtelectedUsers option').each(function (){
        var option = $(this);
        $('#nieuweKlachtBeschikbareUsersSelect').append(option);
        $("#nieuweKlachtelectedUsers option[value='" + option.text() + "']").remove();
    });
    SortSelect($("#nieuweKlachtBeschikbareUsersSelect > option"), $("#nieuweKlachtBeschikbareUsersSelect"));
    $('#nieuweKlachtWerkenTextArea').val("");
}
