﻿"use strict";
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.user = be.itc.sp.user || {};

be.itc.sp.user = (function (user) {
    var _users = [];
    user.getUsers = function () {
        return _users;
    }
    user.setUsers = function (users) {
        _users = users;
    }
    // Zoekt alle User objecten met behulp van de meegeven
    // lijst van namen. 
    // Waneer invert true worden enkel de
    // User object teruggegeven die niet in de nameList zitten
    // Wanneer invert false of niet meegegeven worden de User 
    // objecten teruggegeven die in nameList zitten
    //
    // parameter nameList: ArrayList van namen (User.Titel)
    // parameter invert: boolean 
    // return users:     ArrayList van User objecten
    user.getUsersFromNameList = function (nameList, invert) {
        var users = $.grep(_users, function (e) {
            for (var i = 0; i < nameList.length; i++) {
                return e.Titel === nameList[i];
            }
        }, invert);
        return users;
    }
    // Zoekt alle User objecten met behulp van de meegeven
    // lijst van Id's. 
    // Waneer invert true worden enkel de
    // User object teruggegeven die niet in de idList zitten
    // Wanneer invert false of niet meegegeven worden de User 
    // objecten teruggegeven die in idList zitten
    //
    // parameter idList: ArrayList van UserId's
    // parameter invert: boolean 
    // return users:     ArrayList van User objecten
    user.getUsersFromIdList = function (idList, invert) {
        var users = [];
        var i;
        var f = function(e) {return e.ID == idList[i];}
        for (i = 0; i < idList.length; ++i) {
            var user = $.grep(_users, f)[0];
            users.push(user);
        }
        var users = $.grep(_users, function (e) {
            for (var i = 0; i < idList.length; i++) {
                
            }
        }, invert);
        return users;
    }
    return user;
})(be.itc.sp.user || {});
