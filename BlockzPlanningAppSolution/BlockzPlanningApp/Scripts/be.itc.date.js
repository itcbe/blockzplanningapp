﻿"use strict";
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.date = be.itc.date || {};

be.itc.date = (function (date) {
    // Geeft datum in iCal formaat terug
    // Tijd moet in UTC tijdzone gezet worden
    // return: String!!
    date.toICalFormat = function (date) {
        var _date = new Date();
        if (date != undefined) {
            _date = date
        }
        var d = [];
        var i = -1;
        d[++i] = be.itc.number.pad(_date.getUTCFullYear(), 4);
        d[++i] = be.itc.number.pad(_date.getUTCMonth() + 1, 2);
        d[++i] = be.itc.number.pad(_date.getUTCDate(), 2);
        d[++i] = "T";
        d[++i] = be.itc.number.pad(_date.getUTCHours(), 2);
        d[++i] = be.itc.number.pad(_date.getUTCMinutes(), 2);
        d[++i] = be.itc.number.pad(_date.getUTCSeconds(), 2);
        d[++i] = "Z";

        return d.join('');
    };
    // Geeft de tijd terug in JSON EWS formaat: "2014-02-02T18:00:00-08:00"
    //
    // parameter date: datum/tijd die moet worden omgezet
    //                 gebruikt huidige tijd wanneer die ontbreekt.
    // return: datum in gewenste string format
    date.toJSONEWSFormat = function (date) {
        var _date = new Date();
        if (date != undefined) {
            _date = date
        }
        // geeft het verschil met de UTC datum in minuten
        var offsetMinutes = _date.getTimezoneOffset();
        var offsetSign;
        if (offsetMinutes < 0) {
            offsetSign = "-";
            offsetMinutes = offsetMinutes * -1;
        } else {
            offsetSign = "+";
        }

        // bereken het aantal uren verschil met de UTC datum
        var offsetHours = Math.floor(offsetMinutes / 60);
        // bereken de rest
        offsetMinutes = offsetMinutes - (60 * offsetHours);
        var d = [];
        var i = -1;
        d[++i] = be.itc.number.pad(_date.getFullYear(), 4);
        d[++i] = "-";
        d[++i] = be.itc.number.pad(_date.getMonth() + 1, 2);
        d[++i] = "-";
        d[++i] = be.itc.number.pad(_date.getDate(), 2);
        d[++i] = "T";
        d[++i] = be.itc.number.pad(_date.getHours(), 2);
        d[++i] = ":";
        d[++i] = be.itc.number.pad(_date.getMinutes(), 2);
        d[++i] = ":";
        d[++i] = be.itc.number.pad(_date.getSeconds(), 2);
        d[++i] = offsetSign;
        d[++i] = be.itc.number.pad(offsetHours, 2);
        d[++i] = ":";
        d[++i] = be.itc.number.pad(offsetMinutes, 2);

        return d.join('');
    };
    // Geeft de huidige tijd terug in stappen van 15 minuten
    // rond af naar beneden, dus 12:29 wordt 15!
    //
    // parameter date: datum/tijd waarvan het kwartier moet worden berekend
    //                 gebruikt huidige tijd wanneer die ontbreekt.
    // return: number met als waarde 0, 15, 30 of 45
    date.getQuarterHour = function (date) {
        var _date = new Date();
        if (date != undefined) {
            _date = date
        }
        return Math.floor(_date.getMinutes() / 15) * 15
    }

    return date;
})(be.itc.date || {});
