﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var firstTime = true;
var ddlText, ddlValue, ddl, lblMesg;
var drempelwaardeTicket, drempelwaardeTijdsregistratie;
var appweburl, hostweburl;

// jQuery 1.9 is out… and $.browser has been removed – a fast workaround
jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase()) && !/trident/.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    document.getElementById("ctl00_onetidHeadbnnr2").src = "../Images/Achtergrond_INNI.png";
    document.getElementById("ctl00_onetidHeadbnnr2").width = 120;
    document.getElementById("ctl00_onetidHeadbnnr2").className = "ms-emphasis";
    $('#interventiePopup').hide();
    $.datepicker.setDefaults($.datepicker.regional["nl"]);
    var date = new Date();

    $('#zoektxt').focus(function () {
        $('#zoektxt').val("");
    });

    var datep = $(".datepicker");
    datep.datepicker({
        'dateFormat': 'dd/mm/yy'
    });
    $(".registratiedatepicker").datepicker({ 'dateFormat': 'dd/mm/yy' });
    datep.each(function () {
        var $d = $(this);
        $d.val(ToDateString(date.toString(), true));
    });

    $("#zoektxt").bind("mouseup", function () {
        FilterItems("");
    });
    appweburl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));
    hostweburl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));
    $("#fileInput").append(be.itc.sp.rest.fileupload.getFileInput());
    var getWerkgebieden = GetWerkgebieden();
    getWerkgebieden.done(function () {
        getUserName();
    });
});

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    $('#message').text('Hallo ' + user.get_title());
    $('#uitvoerderLabel').text(user.get_title());
    $("#validatieDialog").prop('title', "Opgelet " + user.get_title());
    $("#validatieDialog").dialog({
        modal: true,
        autoOpen: false,
        minWidth: 400,
        hide: {
            effect: "explode",
            duration: 1000
        },
        buttons: {
            Ok: function () {
                $(this).dialog("close");
                $("#validatieDialog").empty();
            }
        }
    });
    GetStatussen();
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function OpenKlant() {
    var klantnummer = $("#klantenSelect").val();
    if (klantnummer != '') {
        var url = hostweburl + '/Lists/Klanten/Editform.aspx?ID=' + klantnummer;// + '&RootFolder=&IsDlg=1';
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
    else
        alert("Kies eerst een klant!");
}

function Openticketnummer() {
    var id = $("#ticketnummertxt").val();
    if (id !== "") {
        var url = hostweburl + '/Lists/Ticket/Dispform.aspx?ID=' + id;
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
}


function klantselect_onchange() {
    RemoveOldData();

    var klantnummer = $("#klantenSelect").val();
    var klantnaam = $("#klantenSelect :selected").text();
    $('#klantNaamLabel').text(klantnaam);
    SetSelectedKlantToPopups(klantnummer);
    GetTicketsByKlant(klantnummer);
}

function RemoveOldData() {
    $("#recentThead").empty();
    $("#recentTbody").empty();
    $("#klachtenThead").empty();
    $("#klachtenTbody").empty();
    $("#computersThead").empty();
    $("#computersTbody").empty();
    $("#centraleThead").empty();
    $("#centraleTbody").empty();
    $("#registratiesThead").empty();
    $("#registratiesTbody").empty();

}

function SetSelectedKlantToPopups(klantnummer) {
    $("#NieuwTicketKlantSelect").val(klantnummer);

    $("#SelectedKlantSelect").empty();

    $("#ContactKlantSelect").val(klantnummer);
    MoveToSelected();
}

function validateOmschrijving() {
    var tekst = $('#NieuwTicketOmschrijvingtxt').val();
    if (tekst.length > 40)
        alert("De korte omschrijving is te lang! max 40 karakters!");
}

function ShowNieuweKlantPopup() {
    $("#NieuweKlantPopUp").dialog({
        width: 700,
        modal: true,
        maxHeight: 700,
        buttons: [
            {
                text: "Opslaan",
                click: function () {
                    GetKlantFromForm();
                }
            },
            {
                text: "Annuleren",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]

    });
}

function CloseNieuweKlantPopup() {
    $("#NieuweKlantPopUp").dialog("close");
}

function ShowNieuwContactPopup() {
    var klantid = $('#klantenSelect').val();
    if (!klantid || klantid != 0) {
        $('#ContactKlantSelect').val(klantid);
        $("#NieuwContactPopUp").dialog({
            width: 700,
            modal: true,
            maxHeight: 700,
            buttons: [
                {
                    text: "Opslaan",
                    click: function () {
                        GetContactFromForm();
                    }
                },
                {
                    text: "Annuleren",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    }
    else
        alert("Kies eerst een klant!");
}

function CloseNieuwContactPopup() {
    $("#NieuwContactPopUp").dialog("close");
}

function ShowNieuwTicketPopup() {
    var klantid = $('#klantenSelect').val();
    if (!klantid || klantid != 0) {
        $('#NieuwTicketKlantSelect').val(klantid);
        GetContactenForNieuwTicket(klantid, "");
        $("#NieuwTicketPopUp").dialog({
            width: 800,
            modal: true,
            maxHeight: 700,
            buttons: [
                {
                    text: "Opslaan",
                    click: function () {
                        GetTicketFromForm();
                    }
                },
                {
                    text: "Annuleren",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    }
    else
        alert("Kies eerst een klant!");
}

function CloseNieuwTicketPopup() {
    $(".overlay").addClass("Hidden");
    $("#NieuwTicketPopUp").addClass("Hidden");
    DeleteNieuwTicketData();
}

function ShowNieuweKlachtPopup() {
    var klantid = $('#klantenSelect').val();
    if (!klantid || klantid != 0) {
        $('#nieuweKlachtKlantSelect').val(klantid);
        GetContactenForNieuwTicket(klantid, "Klachten");
        $('#NieuweKlachtPopup').dialog({
            width: 800,
            modal: true,
            maxHeight: 700,
            buttons: [
                {
                    text: "Opslaan",
                    click: function () {
                        GetKlachtFromForm();
                    }
                },
                {
                    text: "Annuleren",
                    click: function () {
                        $(this).dialog("close");
                    }

                }
            ]
        });
    }
    else
        alert("Kies eerst een klant!");
}

function CloseNieuweKlachtPopup() {
    $('#NieuweKlachtPopup').dialog("close");
    ClearKlachtForm();
}

function OpenGroupPopUp() {
    $(".overlay").removeClass("Hidden");
    $("#GroepPopup").removeClass("Hidden");
}

function HideGroupPopup() {
    $(".overlay").addClass("Hidden");
    $("#GroepPopup").addClass("Hidden");
}

function MoveToSelected() {
    var option = $("#ContactKlantSelect option:selected").clone();
    $("#SelectedKlantSelect").append(option);
}

function MoveToAvailable() {
    var option = $("#SelectedKlantSelect option:selected");
    option.remove();
}

function MovetechniekerToSelected() {
    var option = $("#tecniekerBeschikbaarSelect option:selected");
    $("#techniekerGekozenSelect").append(option);
    $("#tecniekerBeschikbaarSelect option:selected").remove();
}

function MovetechniekerToAvailable() {
    var option = $("#techniekerGekozenSelect option:selected");
    $("#tecniekerBeschikbaarSelect").append(option);
    $("#techniekerGekozenSelect option:selected").remove();
}

function KlantZoekTxt_KeyUp() {
    var zoektext = $("#zoektxt").val().toLowerCase();
    FilterItems(zoektext);
}

function CacheItems() {
    ddlText = new Array();
    ddlValue = new Array();
    ddl = document.getElementById("klantenSelect");
    lblMesg = document.getElementById("MessageLabel");
    for (var i = 0; i < ddl.options.length; i++) {
        ddlText[ddlText.length] = ddl.options[i].text;
        ddlValue[ddlValue.length] = ddl.options[i].value;
    }
}

function FilterItems(value) {
    ddl.options.length = 0;
    //AddItem("<-- kies een klant -->", 0);
    for (var i = 0; i < ddlText.length; i++) {
        if (ddlText[i].toLowerCase().indexOf(value) != -1) {
            AddItem(ddlText[i], ddlValue[i]);
        }
    }

    if (ddl.options.length == 0) {
        AddItem("Geen klanten gevonden.", "");
        lblMesg.innerHTML = "0 klanten gevonden.";
    }
    else if (ddl.options.length > 1) {
        lblMesg.innerHTML = (ddl.options.length) + " klanten gevonden.";
        if ($("#klantenSelect")[0].options[0].value != 0) {
            $("#klantenSelect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
            $("#klantenSelect")[0].options[0].selected = true;
        }
    }
    else {
        lblMesg.innerHTML = (ddl.options.length) + " klant gevonden.";
        if ($("#klantenSelect")[0].options[0].value != 0) {
            $("#klantenSelect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
            $("#klantenSelect")[0].options[0].selected = true;
        }
    }
    //CurrentKlantId = $("#KlantNummerHiddenField").val();//document.getElementById("").value;
}

function AddItem(text, value) {
    var opt = document.createElement("option");
    opt.text = text;
    opt.value = value;
    ddl.options.add(opt);
}



function CopyLink(id) {
    var url = 'https://itcbe.sharepoint.com/Lists/Ticket/Dispform.aspx?ID=' + id;
    //e.preventDefault();
    if (window.clipboardData) {
        window.clipboardData.setData('Text', url);
        alert("De URL is op het klembord geplaatst.");
    } else {
        window.prompt("Kopieer naar klembord: Ctrl+C, Enter", url);
    }
    return false;
}

function RecentGeslotenButton_Click() {
    var klantid = $("#klantenSelect").val();
    var dagen = $("#dagentxt").val();
    if (dagen !== '') {
        var currentdate = new Date();
        var zoekdatum = new Date(currentdate);
        zoekdatum.setDate(currentdate.getDate() - dagen);
        var datum = ToSharePointDate(SystemdateToString(zoekdatum));
        GetRecentGesloten(klantid, datum);
    }
    else
        alert("Er is geen getal ingegeven bij aantal dagen terug!");
}

function KlachtenButton_Click() {
    var klantid = $("#klantenSelect").val();
    GetKlachten(klantid);
}

function ComputerButton_Click() {
    var klantid = $("#klantenSelect").val();
    GetComputers(klantid);
}

function TelefooncentraleButton_Click() {
    var klantid = $("#klantenSelect").val();
    GetTelefooncentrates(klantid);
}

function TijdsregistratieButton_Click() {
    GetAllRegistraties();
}


/**************************** Exporteren van de tickets, tijdsregistraties ********************/
function ExportTicketButton_Click() {
    var ticketArray = [];
    var headers = [];
    $("#ticketThead th").each(function (index, item) {
        headers[index] = $(item).text();
    });

    $("#ticketTbody tr").each(function () {
        //var arrayOfThisRow = [];
        var arrayItem = {};
        var tableData = $(this).find('td');
        if (tableData.length > 1) {

            tableData.each(function (index, item) {
                arrayItem[headers[index]] = $(item).text();//.push($(this).text());
            });
            ticketArray.push(arrayItem);
        }
    });
    var csv = arrayToCSV(ticketArray);
    DownloadFile(csv);
}

function ExportRegistratiesButton_Click() {
    var registratiesArray = [];
    var headers = [];
    $("#registratiesThead th").each(function (index, item) {
        headers[index] = $(item).text();
    });

    $("#registratiesTbody tr").each(function () {
        //var arrayOfThisRow = [];
        var arrayItem = {};
        var tableData = $(this).find('td');
        if (tableData.length > 1) {

            tableData.each(function (index, item) {
                arrayItem[headers[index]] = $(item).text();//.push($(this).text());
            });
            registratiesArray.push(arrayItem);
        }
    });
    var csv = arrayToCSV(registratiesArray);
    DownloadFile(csv);
}

function DownloadFile(file) {
    var blob = new Blob([file], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) {
        navigator.msSaveBlob(blob, 'PlanningAppLijst.csv');
    }
    else {

        var encodedUri = URL.createObjectURL(blob);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "PlanningAppLijst.csv");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}

/************************* Filter ticketlijst op Technisch, Software of alles *******************************/
function TicketFilterRadio_CheckChanged() {
    var filtertext = "";
    var radio = $("input[name='ticketfilter']:checked");
    if (radio[0].id === "TechnischTicketRadio") {
        filtertext = "Technisch"
    }
    else if (radio[0].id === "SoftwareTicketRadio") {
        filtertext = "Software"
    }
    else {
        filtertext = "Alle";
    }

    var tabel = $("#ticketTbody tr");
    if (filtertext == "Alle") {
        tabel.show();
        return;
    }

    tabel.hide();

    tabel.filter(function (i, v) {
        var $t = $(this);
        var afdeling = $t.find("td:nth-child(9)").text();
        if (afdeling.toLowerCase().indexOf(filtertext.toLowerCase()) >= 0) {
            return true;
        }
        return false;
    }).show();
}

/*********************** Filter de computerlijst op Ticket aangemaakt *********************************/
function ComputerFilter_CheckChanged() {
    var filtertext = "";
    var radio = $("input[name='computerfilter']:checked");
    if (radio[0].id === "ZonderTicketRadio") {
        filtertext = "Nee"
    }
    else if (radio[0].id === "MetTicketRadio") {
        filtertext = "Ja"
    }
    else {
        filtertext = "Alle";
    }

    var tabel = $("#computersTbody tr");
    if (filtertext == "Alle") {
        tabel.show();
        return;
    }

    tabel.hide();

    tabel.filter(function (i, v) {
        var $t = $(this);
        var aangemaakt = $t.find("td:nth-child(7)").text();
        if (aangemaakt.toLowerCase().indexOf(filtertext.toLowerCase()) >= 0) {
            return true;
        }
        return false;
    }).show();
}

// 
function TicketIsDringend() {
    if ($("#NieuwTicketIsDringend").prop('checked')) {
        $("#NieuwTicketDringendOpmerkingRow").removeClass("Hidden");
        return true;
    } else {
        $("#NieuwTicketDringendOpmerkingRow").addClass("Hidden");
        return false;
    }
}

function InterventieUitvoerderSelect_Change() {
    $('#uitvoerderLabel').text($('#uitvoerderSelect :selected').text());
}

function InterventieDatumTxt_Change() {
    var vandaag = $('#datumLabel').text();
    var gekozendatum = $('#interventieDatumTxt').val();
    $('#datumLabel').text(gekozendatum);

    $('#interventieTBody tr').each(function () {
        var tr = $(this);
        var datumcell = tr.find('td:nth-child(4)');
        if (datumcell.text() === vandaag)
            datumcell.text(gekozendatum);
    });

}

function ClearFileUpload() {
    $("#attachmentList").empty();
    $("#fileList").empty();
    $("#addFileList").empty();
    $("#fileInput").empty();
    $("#fileInput").append(be.itc.sp.rest.fileupload.getFileInput());
}