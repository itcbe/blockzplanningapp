﻿"use strict";
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.rest = be.itc.sp.rest || {};

var ns = be.itc.sp.rest;

ns.getListItemById = function (list, id) {
    var deferred = $.Deferred();
    $.getScript(be.itc.sp.hostweburl + "/_layouts/15/SP.RequestExecutor.js", runCrossDomainRequest);
    return deferred.promise();

    function runCrossDomainRequest() {
        var url = String.format("{0}/_api/SP.AppContextSite(@target)/web/lists/getbyTitle('{1}')/Items({2})?@target='{3}'",
            be.itc.sp.appweburl, list, id, encodeURIComponent(be.itc.sp.hostweburl));
        var executor = new SP.RequestExecutor(be.itc.sp.appweburl);
        executor.executeAsync(
            {
                url: url,
                method: "GET",
                headers:
                    {
                        "Accept": "application/json; odata=verbose"
                    },
                success: successHandler,
                error: errorHandler
            }
        );
    }

    function successHandler(data) {
        deferred.resolve(JSON.parse(data.body).d);
    }

    function errorHandler(data) {
        deferred.reject(data);
    }
};
