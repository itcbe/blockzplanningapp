﻿"use strict";
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.planning = be.itc.sp.planning || {};
be.itc.sp.planning.appointment = be.itc.sp.planning.appointment || {};

be.itc.sp.planning.appointment = (function (appointment) {
    var _toegewezenAan;
    var _subject;
    var _location;
    var _currentDate;
    var _startDate;
    var _endDate;
    var _content;
    var _selectedUsers;
    var _allDayEvent

    appointment.getToegewezenAan = function () {
        return _toegewezenAan;
    };
    appointment.getSubject = function () {
        return _subject;
    };
    appointment.getLocation = function () {
        return _location;
    };
    appointment.getCurrentDate = function () {
        return _currentDate;
    };
    appointment.getStartDate = function () {
        return _startDate;
    };
    appointment.getEndDate = function () {
        return _endDate;
    };
    appointment.getContent = function () {
        return _content;
    };
    appointment.getSelectedUsers = function () {
        return _selectedUsers;
    }
    appointment.getAllDayEvent = function () {
        return _allDayEvent;
    }

    var setAppointmentDialogFormFields = function (ticket, klant) {
        // toegewezenAan bevat een ArrayList met alle ID's van toegewezen User objecten
        _toegewezenAan = ticket.Toegewezen_x0020_aanId.results;
        _subject = String.format("{0} {1} {2} - {3}", ticket.Type_x0020_ticket, ticket.Id, klant.Naam1, ticket.Title);
        _location = String.format("{0}, {1} {2}", klant.Straat_x0020__x002b__x0020_numme, klant.Postcode, klant.Locatie, klant.Telefoon);
        _currentDate = new Date();
        _startDate = new Date();
        _endDate = new Date();
        // rond minuten af tot op het vorige kwartuur
        _startDate.setMinutes(be.itc.date.getQuarterHour(_startDate));
        _endDate.setMinutes(be.itc.date.getQuarterHour(_endDate));
        // zet seconden op null;
        _startDate.setSeconds(0);
        _startDate.setMilliseconds(0);
        _endDate.setSeconds(0);
        _endDate.setMilliseconds(0);

        _content = String.format("<div contentEditable='false'>" +
            "<a href='{1}/Lists/Ticket/Dispform.aspx?ID={0}' target='_blank'>Open Ticket {0}</a><br>" +
            "<a href='{1}/Lists/Ticket/Editform.aspx?ID={0}' target='_blank'>Bewerk Ticket {0}</a>" +
            "</div><br />", ticket.Id, be.itc.sp.getHostWebUrl());


        var dialog = $("#PopUpContainer").dialog({
            title: "Afspraak maken",
            autOpen: false,
            height: "auto",
            maxHeight: $(window).height() * 0.95,
            width: "auto",
            modal: true,
            overflow: "auto",
            buttons: {
                "Opslaan": appointment.createAppointment,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
            }
        });

        var form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            appointment.createAppointment;
        });

        setUserSelect(_toegewezenAan, "appointmentTechniekerBeschikbaarSelect", true);
        setUserSelect(_toegewezenAan, "appointmentTechniekerGekozenSelect", false);


        $("#appointmentOnderwerpTextBox").val(_subject);
        $("#appointmentLocatieTextBox").val(_location);

        $("#appointmentStartDatumTextBox").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
        $("#appointmentStartDatumTextBox").datepicker("setDate", _startDate);
        $("#appointmentStartDatumTextBox").on("change", function (e) {
            _startDate = $(".datepicker[name=appointmentStartDatumTextBox]").datepicker('getDate');
            _startDate.setHours($("#appointmentStartUurDropDownList").val());
            _startDate.setMinutes($("#appointmentStartMinuutDropDownList").val());
        });
        $("#appointmentStartUurDropDownList").val(be.itc.number.pad(_startDate.getHours(), 2));
        $("#appointmentStartMinuutDropDownList").val(be.itc.number.pad(_startDate.getMinutes(), 2));
        $("#appointmentEindDatumTextBox").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
        $("#appointmentEindDatumTextBox").datepicker("setDate", _endDate);
        $("#appointmentEindDatumTextBox").on("change", function (e) {
            _endDate = $(".datepicker[name=appointmentEindDatumTextBox]").datepicker('getDate');
            _endDate.setHours($("#appointmentEindUurDropDownList").val());
            _endDate.setMinutes($("#appointmentEindMinuutDropDownList").val());
        });
        $("#appointmentEindUurDropDownList").val(be.itc.number.pad(_endDate.getHours(), 2));
        $("#appointmentEindMinuutDropDownList").val(be.itc.number.pad(_endDate.getMinutes(), 2));

        $("#appointmentMorningCheckBox").on("click", function (event) {
            setTime("08", "00", "12", "00");
        });
        $("#appointmentAfterNoonCheckBox").on("click", function (event) {
            setTime("13", "00", "17", "00");
        });
        $("#appointmentWholeDayCheckBox").on("click", function (event) {
            setTime("08", "00", "17", "00");
        });

        $("#appointmentInhoudTextBox").html(_content);
        $("#appointmentInhoudTextBox").on("change", function (event) {
            
        });
        dialog.dialog("open");

    }
    // Vult de select objecten met User objecten. 
    // paramter idList:    lijst van UserId's die geselecteerd zijn voor deze appointment
    // parameter selectId: het id van het select dom-element dat moet gevuld worden
    //                     met User objecten
    // parameter selected: true vult de lijst met geselecteerde Users
    //                     false vult de lijst met beschikbare Users
    var setUserSelect = function (idList, selectId, selected) {
        var userlist = be.itc.sp.user.getUsersFromIdList(idList, selected);
        if (!selected) {
            _selectedUsers = userlist;
        }
        for (var i = 0; i < userlist.length; i++) {
            var option = "<option value='" + userlist[i].ID + "'>" + userlist[i].Titel + "</option>";
            $("#" + selectId).append(option);
        }
    };
    // Vult de beginuur en einduur velden
    var setTime = function (beginHour, beginMinute, endHour, endMinute) {
        $("#appointmentStartUurDropDownList").val(beginHour);
        $("#appointmentStartMinuutDropDownList").val(beginMinute);
        $("#appointmentEindUurDropDownList").val(endHour);
        $("#appointmentEindMinuutDropDownList").val(endMinute);
        _startDate.setHours(beginHour);
        _startDate.setMinutes(beginMinute);
        _endDate.setHours(endHour);
        _endDate.setMinutes(endMinute);
    };

    var updateSelectedUsers = function () {
        var selectedUserIds = [];
        $("#appointmentTechniekerGekozenSelect option").each(function () {
            selectedUserIds.push($(this).val());
        });
        _selectedUsers = be.itc.sp.user.getUsersFromIdList(selectedUserIds, false);
        var test = _selectedUsers.length;
    }

    var getParticipantsPicker = function () {
        var users = [];
        for (var i = 0; i < _selectedUsers.length; ++i) {
            users.push(SP.FieldUserValue.fromUser(_selectedUsers[i].LoginName));
        }
        return users;
    };

    appointment.moveTechniciansToSelected = function () {
        var option = $("#appointmentTechniekerBeschikbaarSelect option:selected");
        $("#appointmentTechniekerGekozenSelect").append(option);
        $("#appointmentTechniekerBeschikbaarSelect option:selected").remove();
    }

    appointment.moveTechniciansToAvailable = function () {
        var option = $("#appointmentTechniekerGekozenSelect option:selected");
        $("#appointmentTechniekerBeschikbaarSelect").append(option);
        $("#appointmentTechniekerGekozenSelect option:selected").remove();
    }

    appointment.openAppointmentDialog = function (id) {
        $("#PopUpContainer").load("../Pages/Forms/appointment.html", function () {
            var ticketQuery = be.itc.sp.ticket.getTicketById(id);
            ticketQuery.done(function (ticket) {
                var klantQuery = be.itc.sp.klant.getKlantById(ticket.KlantId);
                klantQuery.done(function (klant) {
                    setAppointmentDialogFormFields(ticket, klant);
                });
                klantQuery.fail(function (e) {
                    alert(e);
                })
            });
            ticketQuery.fail(function (e) {
                alert(e);
            });
        });
    }

    appointment.createAppointment = function () {
        // Geeft sharepoint app site terug ipv de gewone sharepoint site
        //var context = new SP.ClientContext.get_current();
        //var list = context.get_web().get_lists().getByTitle("Test Planning Agenda");
        // 
        _content = $("#appointmentInhoudTextBox").html();
        updateSelectedUsers();

        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle("Test Planning Agenda");

        var newEventCreateInfo = new SP.ListItemCreationInformation();
        var test = newEventCreateInfo.customFromJson();
        var newEvent = list.addItem(newEventCreateInfo);
        newEvent.set_item("Location", appointment.getLocation());
        newEvent.set_item("EventDate", appointment.getStartDate());
        newEvent.set_item("EndDate", appointment.getEndDate());
        newEvent.set_item("Description", appointment.getContent());
        newEvent.set_item("Title", appointment.getSubject());
        // Is dit ok??? hele dag = 00:00 tot 23:59:59
        newEvent.set_item("fAllDayEvent", $("input[name=appointmentTimeRadio]:checked").attr('id') == 'appointmentWholeDayCheckBox');
        newEvent.set_item("Category", "Discussion");
        newEvent.set_item("ParticipantsPicker", getParticipantsPicker());
        newEvent.update();
        context.executeQueryAsync(function () {
            alert("Success");
        }, function (e1, e2, e3, e4) {
            alert("Failure");
        })
    };

    appointment.createAppointmentJSON = function () {
        updateSelectedUsers();
        var url = "http://outlook.office365.com/api/v1.0/me/events";
        var urlCalendar = "https://outlook.office365.com/api/v1.0/me/calendars/Agenda/events";
        // version = v1.0 of beta
        // calender id = nog uit te zoeken

        var contentType = "Content-Type: application/json";
        var valid = true;
        var attendees = [];
        for (var i = 0; i < appointment.getSelectedUsers().length; ++i) {
            var emailaddress = {
                "EmailAddress": {
                    "Address": appointment.getSelectedUsers()[i].Email,
                    "Name": appointment.getSelectedUsers()[i].Titel
                },
                "Type": "Required"
            }
            attendees.push(emailaddress);
        }
        var event = {
            "Subject": appointment.getSubject(),
            "Body": {
                "ContentType": "HTML",
                "Content": appointment.getContent()
            },
            "Start": appointment.getStartDate().toJSON(),
            //"StartTimeZone": "Pacific Standard Time",
            "End": appointment.getEndDate().toJSON(),
            //"EndTimeZone": "Pacific Standard Time",
            "Attendees": attendees
        }
        alert(JSON.stringify(event));
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(event),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Accept": "application/json; odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (data, status, xhr) {
                alert("Ok");
            },
            error: function (xhr, status) {
                alert("NOK");
            }
        });
        return valid;
    };

    appointment.createAppointmentiCal = function () {
        var valid = true;
        var j = -1
        var icsMSG = [];
        icsMSG[++j] = "BEGIN:VCALENDAR";
        icsMSG[++j] = "\n";
        icsMSG[++j] = "PRODID:-//ITC.BE//Planning App 1.0.2.0//EN";
        icsMSG[++j] = "\n";
        icsMSG[++j] = "VERSION:2.0";
        icsMSG[++j] = "\n";
        icsMSG[++j] = "CALSCALE:GREGORIAN";
        icsMSG[++j] = "\n";
        icsMSG[++j] = "METHOD:REQUEST";
        icsMSG[++j] = "\n";
        icsMSG[++j] = "BEGIN:VEVENT";
        icsMSG[++j] = "\n";
        icsMSG[++j] = "DTSTART:";
        icsMSG[++j] = be.itc.date.toICalFormat(appointment.getStartDate());
        icsMSG[++j] = "\n";
        icsMSG[++j] = "DTEND:";
        icsMSG[++j] = be.itc.date.toICalFormat(appointment.getEndDate());
        icsMSG[++j] = "\n";
        icsMSG[++j] = "DTSTAMP:";
        icsMSG[++j] = be.itc.date.toICalFormat(appointment.getCurrentDate());
        icsMSG[++j] = "\n";

        for (var i = 0; i < appointment.getSelectedUsers().length; ++i) {
            var user = appointment.getSelectedUsers()[i];
            icsMSG[++j] = "ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=";
            icsMSG[++j] = user.Titel;
            icsMSG[++j] = ";X-NUM-GUESTS=0:mailto:";
            icsMSG[++j] = user.Email;
            icsMSG[++j] = "\n";
        };
        icsMSG[++j] = "DESCRIPTION:";
        icsMSG[++j] = appointment.getContent();
        icsMSG[++j] = "\n";
        icsMSG[++j] = "LOCATION:";
        icsMSG[++j] = appointment.getLocation();
        icsMSG[++j] = "\n";
        icsMSG[++j] = "SUMMARY:";
        icsMSG[++j] = appointment.getSubject();
        icsMSG[++j] = "\n";

        icsMSG[++j] = "END:VEVENT";
        icsMSG[++j] = "\n";
        icsMSG[++j] = "END:VCALENDAR";

        alert(icsMSG.join(''));
        window.open("data:text/calendar;charset=utf8," + escape(icsMSG.join('')));

        return valid;
    }

    return appointment;
})(be.itc.sp.planning.appointment || {});
