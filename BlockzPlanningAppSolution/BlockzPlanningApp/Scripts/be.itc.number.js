﻿"use strict";
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.number = be.itc.number || {};

be.itc.number = (function (number) {
    // pad functie
    // zet number om in string met voorloop padding
    // wanneer padding niet wordt meegegeven wordt het getal
    // met voorloop nullen gevuld
    // de string zal de breedte van de meegegeven width krijgen
    //
    // parameter number:  om te zetten number variabele
    // parameter width:   breedte van de uiteindelijke string
    // parameter padding: karakter waarmee de string wordt opgevuld
    //                    om de gewenste breedte te krijgen.
    //                    standaardwaarde: 0
    // return: String met number en voorloop padding
    number.pad = function (number, width, padding) {
        padding = padding || '0';
        number = number + '';
        return number.length >= width ? number : new Array(width - number.length + 1).join(padding) + number;
    }

    return number;
})(be.itc.number || {});
