﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link rel="Stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/jquery-ui-1.11.4.min.js"></script>
    <script src="/_layouts/15/sp.runtime.js"></script>
    <script src="/_layouts/15/sp.js"></script>
    <script src="../Scripts/jquery.dataTables.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.widgets.min.js"></script>
    <script src="../Scripts/DatumFuncties.js"></script>
    <!--<script src="https://appsforoffice.microsoft.com/lib/1/office.js"></script>-->
<%--    <script src="../Scripts/Office/1/office.js"></script>--%>
    <script src="../Scripts/be.itc.number.js"></script>
    <script src="../Scripts/be.itc.date.js"></script>
    <script src="../Scripts/be.itc.sp.js"></script>
    <script src="../Scripts/be.itc.sp.klant.js"></script>
<%--    <script src="../Scripts/be.itc.sp.planning.appointment.js"></script>--%>
    <script src="../Scripts/be.itc.sp.rest.js"></script>
    <script src="../Scripts/be.itc.sp.ticket.js"></script>
    <script src="../Scripts/be.itc.sp.user.js"></script>
    <script src="../Scripts/Map.js"></script>
    <script src="../Scripts/be.itc.sp.exception.js"></script>
    <script src="../Scripts/be.itc.sp.rest.fileupload.js"></script>
    <script src="../Scripts/Models/WerkgebiedViewModel.js"></script>
    <script src="../Scripts/Models/VertegenwoordigerViewModel.js"></script>
    <script src="../Scripts/Models/SectorViewModel.js"></script>
    <script src="../Scripts/Models/DrempelwaardeViewModel.js"></script>
    <script src="../Scripts/Models/StatusViewModel.js"></script>
    <script src="../Scripts/Models/AfspraakModel.js"></script>
    <script src="../Scripts/Models/UsersViewModel.js"></script>
    <script src="../Scripts/Models/KlantenViewModel.js"></script>
    <script src="../Scripts/Models/InterventieDocumentViewModel.js"></script>
    <script src="../Scripts/Models/DringendeTicketsViewModel.js"></script>
    <script src="../Scripts/Models/TicketViewModel.js"></script>
    <script src="../Scripts/Models/ContactenViewModel.js"></script>
    <script src="../Scripts/Models/RecentGeslotenViexModel.js"></script>
    <script src="../Scripts/Models/KlachtenViewModel.js"></script>
    <script src="../Scripts/Models/ComputersViewModel.js"></script>
    <script src="../Scripts/Models/TelefooncentraleViewModel.js"></script>
    <script src="../Scripts/Models/TijdsregistratieViewModel.js"></script>
    <script src="../Scripts/Models/AfspraakModel.js"></script>
    <script src="../Scripts/ExportArrayToCSV.js"></script>
    <script src="../Scripts/datepicker-nl.js"></script>
    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link href="../Content/appointment.css" rel="stylesheet" />
    <link href="../Content/jquery-ui-theme/jquery-ui.css" rel="stylesheet" />
    <link href="../Content/jquery-ui-theme/jquery-ui.structure.css" rel="stylesheet" />
    <link href="../Content/jquery-ui-theme/jquery-ui.theme.css" rel="stylesheet" />
    <!-- Add your JavaScript to the following file -->
    <script src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Planning
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div>
        <a name="top"></a>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    </div>
    <div>
        <nav>
            <ul>
<%--                <li><a onclick="ShowNieuweKlantPopup();">Nieuwe klant</a></li>
                <li><a onclick="ShowNieuwContactPopup();">Nieuw contact</a></li>
                <li><a onclick="ShowNieuwTicketPopup();">Nieuw ticket</a></li>--%>
                <li><a href="#Tickets">Tickets</a></li>
                <li><a href="#Klachten">Klachten</a></li>
                <li><a href="#RecentGesloten">Recent Gesloten</a></li>
                <li><a href="#Tijdsregistraties">Tijdsregistraties</a></li>
                <li><a onclick="ShowInterventiePopup();">Interventielijst</a></li>
            </ul>
        </nav>
        <%--        <div style="padding: 5px;">
            Zoek:
            <input type="text" id="zoektxt" onkeyup="KlantZoekTxt_KeyUp();" /><span id="MessageLabel" style="margin: 0 15px;"></span>
            Klanten:
            <select id="klantenSelect" onchange="klantselect_onchange();"></select>
            <a onclick="OpenKlant();">
                <img src="../Images/Open.gif" class="icoon" alt="Open klant" /></a>
        </div>--%>
        <div class="InfoDiv">
            <div id="klantDetailsDiv">
                <h3>Klant<a class="Buttons2" onclick="ShowNieuweKlantPopup();">Nieuwe klant</a><a class="Buttons2b" onclick="OpenKlant();">Open klant</a></h3>
                <div class="stap2">
                    <div style="padding: 5px;">
                        Zoek:
                        <input type="text" id="zoektxt" onkeyup="KlantZoekTxt_KeyUp();" /><span id="MessageLabel" style="margin: 0 15px;"></span>
                        Klanten:
                        <select id="klantenSelect" onchange="klantselect_onchange();"></select>
<%--                        <a onclick="OpenKlant();">
                            <img src="../Images/Open.gif" class="icoon" alt="Open klant" />
                        </a>--%>
                    </div>
                    <table>
                        <tbody>
                            <tr>
                                <td class="labels">ID:</td>
                                <td id="IDCell"></td>
                                <td class="labels">Opmerking facturatie:</td>
                                <td class="labels">Opmerking klant:</td>
                            </tr>
                            <tr>
                                <td class="labels">Bedrijf:</td>
                                <td id="bedrijfCell"></td>
                                <td id="facturatieCell" rowspan="6" style="width: 300px;"></td>
                                <td id="opmklantCell" rowspan="3"></td>
                            </tr>
                            <tr>
                                <td class="labels">Adres:</td>
                                <td id="adresCell"></td>
                            </tr>
                            <tr>
                                <td class="labels">Telefoon:</td>
                                <td id="telefoonCell"></td>
                            </tr>
                            <tr>
                                <td class="labels">Vertegenwoordiger:</td>
                                <td id="vertegenwoordigertCell"></td>
                                <td id="opmLocatieCell" rowspan="4" style="width: 300px;"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="contactenDiv">
                <h3>Contacten<a class="Buttons2" onclick="ShowNieuwContactPopup();">Nieuw contact</a></h3>
                <div class="stap2">
                    <table>
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Naam</td>
                                <td>Telefoon</td>
                                <td>GSM</td>
                                <td>E-mail</td>
                            </tr>
                        </thead>
                        <tbody id="contactTbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
    <h3>Tickets<a class="Buttons2" onclick="ShowNieuwTicketPopup();">Nieuw ticket</a><a class="Buttons2b" onclick="klantselect_onchange();">Update tickets</a><a class="Buttons2b" onclick="ExportTicketButton_Click();">Exporteer tickets</a></h3>
    <div class="stap">
        <a name="Tickets" href="#top" class="ankers">Top</a>
        <table>
            <tbody>
                <tr>
                    <td>Open ticket nu:</td>
                    <td>
                        <input type="text" id="ticketnummertxt" /></td>
                    <td><a class="Buttons" onclick="Openticketnummer();">Open</a></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Zoek:</td>
                    <td>
                        <input type="text" id="ticketzoektxt" onkeyup="FilterTickets();" /></td>
                    <td>
                        <input type="radio" checked="checked" name="ticketfilter" id="AlleTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Alle</td>
                    <td>
                        <input type="radio" name="ticketfilter" id="TechnischTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Technisch</td>
                    <td>
                        <input type="radio" name="ticketfilter" id="SoftwareTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Software</td>
                </tr>
            </tbody>
        </table>
        <div id="ticketDiv">
            <table id="ticketTable">
                <thead id="ticketThead">
                    <tr>
                        <th>Nummer</th>
                        <th>Omschrijving</th>
                        <th>Prioriteit</th>
                        <th>Toegewezen aan</th>
                        <th>Contact persoon</th>
                        <th>Opvolgingsdatum</th>
                        <th>Status</th>
                        <th>Werkgebied</th>
                        <th>Afdeling</th>
                        <th>Laatst gewijzigd</th>
                        <th>Door</th>
                    </tr>
                </thead>
                <tbody id="ticketTbody">
                </tbody>
            </table>
        </div>
    </div>
    <h3>Klachten<a class="Buttons2" onclick="ShowNieuweKlachtPopup();">Nieuwe klacht</a><a class="Buttons2b" onclick="KlachtenButton_Click();">Toon</a></h3>
    <div class="stap">
        <p><a name="Klachten" href="#top" class="ankers">Top</a></p>
        
        <table>
            <thead id="klachtenThead">
            </thead>
            <tbody id="klachtenTbody">
            </tbody>
        </table>
    </div>
    <h3>Recent gesloten tickets<a class="Buttons2" onclick="RecentGeslotenButton_Click()">Toon</a></h3>
    <div class="stap">
        <p><a name="RecentGesloten" href="#top" class="ankers">Top</a></p>
        Hoeveel dagen wilt U terug gaan:
        <input type="text" id="dagentxt" />
        <table>
            <thead id="recentThead">
            </thead>
            <tbody id="recentTbody">
            </tbody>
        </table>
    </div>
    <h3>Tijdsregistraties<a class="Buttons2" onclick="TijdsregistratieButton_Click();">Toon</a><a class="Buttons2b" onclick="ExportRegistratiesButton_Click();">Exporteer lijst</a></h3>
    <div class="stap">
        <p><a name="Tijdsregistraties" href="#top" class="ankers">Top</a></p>
        Datum vanaf:
        <input type="text" class="registratiedatepicker" id="vanafDatePicker" style="margin: 0 0 10px 0;" />
        Ticketnummer:
        <input type="text" id="ticketnummerTextBox" style="margin: 0 0 10px 0;" />
        Uitgevoerd door:
        <select id="uitgevoerdDoorSelect" style="margin: 0 0 10px 0;"></select>
        Status:
        <select id="statusSelect" style="margin: 0,0,10px,0;">
<%--            <option></option>
            <option>Nieuw</option>
            <option>Te bespreken</option>
            <option>Wacht - Factureerbaar</option>
            <option>Factureren</option>
            <option>Afgewerkt</option>
            <option>Onafgewerkt</option>
            <option>Onafgewerkt en nagekeken</option>
            <option>Volgende facturatiemaand factureren</option>
            <option>Samen te factureren</option>
            <option>Mail - verdachte registratie</option>
            <option>Later na te kijken</option>--%>
        </select>
        Aflevernota:
        <select id="aflevernotaSelect" style="margin: 0,0,10px,0;">
            <option></option>
            <option>Ja</option>
            <option>Nee</option>
        </select>      
        <table>
            <thead id="registratiesThead">
            </thead>
            <tbody id="registratiesTbody">
            </tbody>
        </table>
    </div>
    <div class="overlay Hidden">
    </div>
    <div id="NieuweKlantPopUp" title="Nieuwe klant" class="Hidden"><%--PopUp --%>
<%--        <img src="../Images/close.gif" alt="Sluiten" class="closeButton" onclick="CloseNieuweKlantPopup();" />
        <h4>Nieuwe klant</h4>--%>
        <div style="overflow-y: auto;">
            <fieldset>
                <span class="labelFor">Nummer *</span><input type="text" id="NieuweKlantNummertxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Bedrijf *</span><input type="text" id="NieuweKlantBedrijftxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Tweede naam</span><input type="text" id="NieuweKlantTweedeNaamtxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Straat *</span>
                <input type="text" id="NieuweKlantAdrestxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Huisnummer *</span>
                <input type="text" id="NieuweKlantHuisnummertxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Postcode *</span>
                <input type="text" id="NieuweKlantPostcodetxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Gemeente *</span>
                <input type="text" id="NieuweKlantLocatietxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Telefoon *</span>
                <input type="text" id="NieuweKlantTelefoontxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Fax</span>
                <input type="text" id="NieuweKlantFaxtxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Email</span>
                <input type="text" id="NieuweKlantEmailtxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Website</span>
                <input type="text" id="NieuweKlantWebsitetxt" class="nieuwTextBox" value="http://" />
            </fieldset>
            <fieldset>
                <span class="labelFor">Vertegenwoordiger *</span>
                <select id="NieuweKlantVertegenwoordigerSelect">
                </select>
            </fieldset>
            <fieldset>
                <span class="labelFor">Sector</span>
                <select id="NieuweKlantSectorSelect">
                </select>
            </fieldset>
            <fieldset>
                <span class="labelFor">Algemene opmerking</span>
                <textarea id="NieuweKlantAlgemeneOpmTextArea" class="nieuwtextarea"></textarea>
            </fieldset>
            <fieldset>
                <span class="labelFor">Opmerking facturatie</span>
                <textarea id="NieuweKlantOpmFactuurtxt" class="nieuwtextarea"></textarea><br />
            </fieldset>
<%--            <fieldset>
                <span class="labelFor"></span>
                <a href="#" class="Buttons" onclick="GetKlantFromForm();">Opslaan</a>
            </fieldset>--%>
        </div>
    </div>
    <div id="NieuwContactPopUp" title="Nieuw Contact" class="Hidden">
<%--        <img src="../Images/close.gif" class="closeButton" alt="Sluiten" onclick="CloseNieuwContactPopup();" />
        <h4>Nieuw contact</h4>--%>
        <div style="overflow-y: auto;">
            <table style="margin: 5px auto;">
                <tbody>
                    <tr>
                        <td>Achternaam *</td>
                        <td>
                            <input type="text" id="ContactNaamtxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Voornaam *</td>
                        <td>
                            <input type="text" id="ContactVoornaamtxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Klant *</td>
                        <td>
                            <%--<div>--%>
                            <select id="ContactKlantSelect"></select>
                            <%--                            </div>
                            <div>
                                <a href="#" onclick="MoveToSelected();" style="margin-left: 120px;">
                                    <img src="../Images/Down.gif" alt="Move down" style="width: 25px;" /></a>
                                <a href="#" onclick="MoveToAvailable();" style="margin-left: 120px;">
                                    <img src="../Images/Up.gif" alt="Move Up" style="width: 25px;" /></a>
                            </div>
                            <div>
                                <select size="4" id="SelectedKlantSelect" class="listbox"></select>
                            </div>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>Functie</td>
                        <td>
                            <input type="text" id="ContactFunctietxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>E-mailadres</td>
                        <td>
                            <textarea id="ContactEmailTextArea" class="nieuwtextarea"></textarea></td>
                    </tr>
                    <tr>
                        <td>Mobiel nummer</td>
                        <td>
                            <input type="text" id="ConatctMobieltxt" value="+32 4" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Telefoon werk</td>
                        <td>
                            <input type="text" id="ContactTelefoonWerktxt" value="+32" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Telefoon thuis</td>
                        <td>
                            <input type="text" id="ContactTelefoonThuistxt" value="+32" class="nieuwTextBox" /></td>
                    </tr>
                    <%--                    <tr>
                        <td>Bedrijf</td>
                        <td>
                            <input type="text" id="ContactBedrijftxt" class="nieuwTextBox" /></td>
                    </tr>--%>
                    <tr>
                        <td>Faxnummer</td>
                        <td>
                            <input type="text" id="ContactFaxtxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Adres</td>
                        <td>
                            <textarea id="ContactAdresTextArea" class="nieuwtextarea"></textarea></td>
                    </tr>
                    <tr>
                        <td>Plaats</td>
                        <td>
                            <input type="text" id="ContactPlaatstxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Provincie</td>
                        <td>
                            <input type="text" id="ContactProvincietxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Postcode</td>
                        <td>
                            <input type="text" id="ContactPostcodetxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Land/regio</td>
                        <td>
                            <input type="text" id="ContactLandtxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Webpagina</td>
                        <td>Typ het webadres:<br />
                            <input type="text" id="ContactWebpaginatxt" value="http://" class="nieuwTextBox" /><br />
                            Typ de beschrijving:<br />
                            <input type="text" id="ContactWebpaginaBeschrijvingtxt" class="nieuwTextBox" />
                        </td>
                    </tr>
                    <tr>
                        <td>Notitie</td>
                        <td>
                            <textarea id="ContactNotitieTextArea" class="nieuwtextarea"></textarea></td>
                    </tr>
                    <tr>
                        <td>Achternaam (fonetisch)</td>
                        <td>
                            <input type="text" id="ContactNaamFontxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Voornaam (fonetisch)</td>
                        <td>
                            <input type="text" id="ContactVoornaamFontxt" class="nieuwTextBox" /></td>
                    </tr>
                    <%--                    <tr>
                        <td>Bedrijfsnaam (fonetisch)</td>
                        <td>
                            <input type="text" id="ContactBedrijfsnaamFontxt" class="nieuwTextBox" /></td>
                    </tr>
                    <tr>
                        <td>Klant oud</td>
                        <td>
                            <select id="ContactKlantOudSelect" style="width: 400px;"></select></td>
                    </tr>
                    <tr>
                        <td>Oud contact</td>
                        <td>
                            <input type="checkbox" id="ContactOudCheckBox" /></td>
                    </tr>--%>
<%--                    <tr>
                        <td></td>
                        <td><a href="#" class="Buttons" onclick="GetContactFromForm();">Opslaan</a></td>
                    </tr>--%>
                </tbody>
            </table>
        </div>
    </div>
    <div id="NieuwTicketPopUp" title="Nieuw ticket" class="Hidden">
<%--        <img src="../Images/close.gif" class="closeButton" alt="Sluiten" onclick="CloseNieuwTicketPopup();" />
        <h4>Nieuw ticket</h4>--%>
        <div style="overflow-y: auto;">
            <input type="hidden" id="NieuweComputerVlag" value="nee" />
            <input type="hidden" id="prijsafspraak" value="" />
            <input type="hidden" id="npIdHidden" value="" />
            <fieldset>
                <label class="labelFor">Klant *</label>
                <select id="NieuwTicketKlantSelect" onchange="LoadContacten();" style="width: 400px;"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor">Contact *</label>
                <select id="NieuwTicketContactSelect" style="width: 400px;"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor">Korte omschrijving *</label>
                <input type="text" id="NieuwTicketOmschrijvingtxt" class="nieuwTextBox" style="margin-bottom: 0px;" onkeyup="validateOmschrijving();" /><br />
                <label class="labelFor"></label>
                <label style="width: 60%; padding: 2px; margin: 0;">Factuur tekst, dus geen "thuis", "privé", telefoonnummer, opmerkingen ...</label>
            </fieldset>
            <fieldset>
                <label class="labelFor">Werkgebied *</label>
                <select id="NieuwTicketWerkgebiedSelect">
                    <%--<option></option>
                    <option>Backup</option>
                    <option>Briljant</option>
                    <option>Citrix</option>
                    <option>Cloud</option>
                    <option>Firewall</option>
                    <option>Hard Disk</option>
                    <option>Hardware</option>
                    <option>Hosting</option>
                    <option>Internet</option>
                    <option>Laptop/desktop</option>
                    <option>Licentie/certificaat</option>
                    <option>Kaseya</option>
                    <option>Mail</option>
                    <option>Mobile</option>
                    <option>Onderhoud</option>
                    <option>Philips Dictate</option>
                    <option>Printer</option>
                    <option>SharePoint</option>
                    <option>Server</option>
                    <option>Sessie lock</option>
                    <option>Software</option>
                    <option>Spam</option>
                    <option>Telecom</option>
                    <option>User account</option>
                    <option>Virus/Spyware</option>
                    <option>Andere</option>
                    <option>Documentatie</option>
                    <option>WerkStatus</option>--%>
                </select>
            </fieldset>
            <fieldset>
                <label class="labelFor">Prioriteit *</label>
                <select id="NieuwTicketPrioriteitSelect">
                    <option>1 Laag</option>
                    <option selected="selected">2 Gemiddeld</option>
                    <option>3 Hoog</option>
                    <option>4 Zeer hoog</option>
                </select>
            </fieldset>
            <fieldset>
                <label class="labelFor">Toegewezen aan *</label>
                <table style="padding-left: 200px;">
                    <tr>
                        <td>
                            <select size="4" id="tecniekerBeschikbaarSelect" class="listbox" style="width: 150px;"></select></td>
                        <td>
                            <a href="#" class="Buttons" onclick="MovetechniekerToSelected();">&gt;&gt;</a><br />
                            <br />
                            <a href="#" class="Buttons" onclick="MovetechniekerToAvailable()">&lt;&lt;</a>
                        </td>
                        <td>
                            <select size="4" id="techniekerGekozenSelect" class="listbox" style="width: 150px;"></select>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <div>
                    <label class="labelFor">Type ticket *</label>
                    <input type="radio" name="typegroup" checked="checked" id="NieuwTicketCallRadio" />CALL<br />
                    <label class="labelFor"></label>
                    <input type="radio" name="typegroup" id="NieuwTicketTaskRadio" />TASK<br />
                    <label class="labelFor"></label>
                    <input type="radio" name="typegroup" id="NieuwTicketProjectRadio" />PROJECT
                </div>
            </fieldset>
            <fieldset>
                <label class="labelFor">Status *</label>
                <select id="NieuwTicketStatusSelect">
<%--                    <option>Gepland</option>
                    <option>Wachten op input klant</option>
                    <option>Wachten op goederen</option>
                    <option>Opvolging</option>
                    <option>Opvolging verlopen</option>
                    <option>Te plannen</option>
                    <option>Afgesloten</option>
                    <option>Wacht op goedkeuring opdracht klant</option>
                    <option>In behandeling</option>
                    <option>Leveren</option>
                    <option>Config</option>--%>
                </select>
            </fieldset>
            <fieldset>
                <label class="labelFor">Opvolgings datum *</label>
                <input type="text" id="NieuwTicketOpvolgDatumtxt" class="datepicker nieuwTextBox" />
            </fieldset>
            <fieldset>
                <label class="labelFor">Voorziene Uren</label>
                <input type="text" id="NieuwTicketUrentxt" class="nieuwTextBox" />
            </fieldset>
            <fieldset>
                <label class="labelFor">Uitgevoerde taken</label>
                <textarea id="NieuwTicketTakenTextArea" class="nieuwtextarea" style="height: 80px;"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor">Bijlagen</label>
                <div style="padding-left: 200px;">
                    <!--File Upload -->
                    <div id="attachmentList"></div>
                    <div id="fileList"></div>
                    <div id="addFileList"></div>
                    <div id="fileInput"></div>
                </div>
            </fieldset>
<%--            <fieldset>
                <div>
                    <label class="labelFor">Afdeling</label>
                    <input type="radio" name="afdelinggroup" checked="checked" id="NieuwTickettechnischRadio" />Technisch<br />
                    <label class="labelFor"></label>
                    <input type="radio" name="afdelinggroup" id="NieuwTicketSoftwareRadio" />Software<br />
                    <label class="labelFor"></label>
                    <input type="radio" name="afdelinggroup" id="NieuwTickettechnischSoftwareRadio" />Technisch en software
                </div>
            </fieldset>--%>
<%--            <fieldset>
                <label class="labelFor"></label>
                <a href="#" class="Buttons" onclick="GetTicketFromForm();">Opslaan</a>
            </fieldset>--%>
        </div>
    </div>
    <div id="NieuweKlachtPopup" class="Hidden" title="Nieuwe klacht">
        <fieldset>
            <label class="labelFor" for="nieuweKlachtKlantSelect">Klant</label>
            <select id="nieuweKlachtKlantSelect" onchange="nieuweKlachtKlanteSelect_OnChange();"></select>
        </fieldset>
        <fieldset>
            <label class="labelFor">Contact</label>
            <select id="nieuweKlachtContactSelect"></select>
        </fieldset>
        <fieldset>
            <label class="labelFor">Korte omschrijving</label>
            <input type="text" id="nieuweKlachtOmschrijvingTxt" class="nieuwTextBox" />
        </fieldset>
        <fieldset>
            <label class="labelFor">Mail/klacht klant</label>
            <textarea id="nieuweKlachtMailTextArea" class="nieuwtextarea"></textarea>
        </fieldset>
        <fieldset>
            <label class="labelFor">Factuurnummer(s)</label>
            <input type="text" class="nieuwTextBox" id="nieuweKlachtFactuurNummersTxt" />
        </fieldset>
        <fieldset>
            <label class="labelFor">Facturatiedatum</label>
            <input type="text" class="datepicker" id="nieuweKlachtFacturatiedatumTxt" />
        </fieldset>
        <fieldset>
            <label class="labelFor">Uitgevoerde werken</label>
            <textarea id="nieuweKlachtWerkenTextArea" class="nieuwtextarea"></textarea>
        </fieldset>
        <fieldset>
            <label class="labelFor">Toegewezen aan</label>
            <div id="multiselectDiv">
                <div>
                    <select id="nieuweKlachtBeschikbareUsersSelect" class="multiselectSelect" size="6"></select>
                </div>
                <div>
                    <a onclick="selecteerUser_click();" class="Buttons">&gt;&gt;</a><br /><br />
                    <a onclick="deselectUser_click();" class="Buttons">&lt;&lt;</a>
                </div>
                <div>
                    <select id="nieuweKlachtelectedUsers" class="multiselectSelect" size="6"></select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <label class="labelFor">Status</label>
            <select id="nieuweKlachtStatusSelect">
                <option>Nieuw</option>
                <option>In behandeling</option>
                <option>Behandeld - moet betaald worden</option>
                <option>Afgesloten</option>
            </select>
        </fieldset>
<%--        <fieldset>
            <label class="labelFor">Factureerder</label>
            <select id="nieuweKlachtFactureerderSelect">
                <option>Optie 1</option>
                <option>Optie 2</option>
            </select>
        </fieldset>--%>
    </div>
    <div id="PopUpContainer"></div>
    <div id="interventiePopup" title="Interventielijst">
<%--        <img src="../Images/close.gif" alt="Sluiten" class="closeButton Hidden" onclick="CloseInterventiePopup();" />--%>
        <div class="scrollable">
            <div>
                <h4>Instellingen</h4>
                <fieldset>
                    <label for="uitvoerderSelect">Uitvoerder</label>
                    <select id="uitvoerderSelect" onchange="InterventieUitvoerderSelect_Change();"></select>
                </fieldset>
                <fieldset>
                    <label for="interventieDatumTxt">Datum</label>
                    <input type="text" id="interventieDatumTxt" onchange="InterventieDatumTxt_Change();" class="datepicker" />
                </fieldset>
                <hr />
            </div>
            <div id="printablediv">
                <div style="align-content: center;">
                    <img style="width: 88px; margin-left: 436px;" src="../Images/itclogosmall.jpg" alt="itc logo" />
                </div>
                <div id="InterventieTitleDiv" style="align-content: center;">
                    <p>Uitgevoerde interventies</p>
                </div>
                <div class="inspringDiv">
                    klant: <span id="klantNaamLabel"></span>
                    <br />
                    Uitvoerder: <span id="uitvoerderLabel"></span>
                    <br />
                    Datum: <span id="datumLabel"></span>
                </div>
                <div class="indentDiv">
                    <input type="checkbox" />
                    Aangemeld om: .............................<br />
                    <input type="checkbox" />
                    Afgemeld om: .............................
                </div>
                <div class="inspringDiv">Interventies:</div>
                <div>
                    <table id="interventieTicketTable">
                        <thead>
                            <tr>
                                <td style="width: 60px;">Ticket</td>
                                <td style="width: 146px;">Korte omschrijving</td>
                                <td style="width: 115px;">Contactpersoon</td>
                                <td style="width: 88px;">Datum</td>
                                <td style="width: 60px;">Uitvoerder</td>
                                <td style="width: 123px;">Status</td>
                                <td style="width: 210px;">Actiepunten</td>
                                <td style="width: 70px;">Handtekening</td>
                            </tr>
                        </thead>
                        <tbody id="interventieTBody">
                        </tbody>
                    </table>
                </div>
                <div style="margin: 10px 0;">Was deze interventie in teken van een onderhoud?</div>
                <div class="indentDiv">
                    <input type="checkbox" />
                    Ja, ik voeg in bijlage de uitgevoerde taken toe.<br />
                    <input type="checkbox" />
                    Nee.
                </div>
                <div style="height: 130px;">
                    Aanvullende informatie / bevindigen bij de interventie:
                </div>
                <div style="height: 100px;">
                    Handtekening klant + naam:
                </div>
                <div style="align-content: center;">
                    <img style="width: 246px; margin-left: 357px;" src="../Images/iconenzondertekst.png" />
                </div>
            </div>
            <div class="buttonDiv">
                <hr />
                <input id="PrintButton" value="Print" type="button" onclick="PrintButton_Click();" />
                <span style="color: red;">Let op! De printerinstellingen dienen nog aangepast te worden naar pagina stand: liggend!!</span>
            </div>
        </div>
    </div>
    <div id="validatieDialog" title="Opgelet!">
    </div>
</asp:Content>
